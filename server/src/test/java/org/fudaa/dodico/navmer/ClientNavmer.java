/*
 * @file         ClientNavmer.java
 * @creation     
 * @modification $Date: 2006-10-19 14:12:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.navmer;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import org.fudaa.dodico.corba.navmer.ICalculNavmer;
import org.fudaa.dodico.corba.navmer.ICalculNavmerHelper;
import org.fudaa.dodico.corba.navmer.SResultatsDAT;

import org.fudaa.dodico.objet.CDodico;

/**
 * @version $Revision: 1.2 $ $Date: 2006-10-19 14:12:28 $ by $Author: deniger $
 * @author Nicolas Maillot
 */
public final class ClientNavmer {
  private ClientNavmer() {}

  public static void main(String[] _argv) {
    ICalculNavmer nv = null;
    // Recherche du serveur Navmer
    nv = ICalculNavmerHelper.narrow(CDodico.findServerByInterface("::navmer::ICalculNavmer", 10000));
    // CDodico.findServerByName("un-serveur-navmer"));
    if (nv == null) {
      System.out.println("connexion au serveur echouee...");
    } else {
      System.out.println("connexion etablie...");
    }
    // A modifier
    /*
     * IParametresNavmer paramsNv = IParametresNavmerHelper.narrow(nv.parametres()); if (paramsNv == null)
     * System.out.println(" paramsN null..."); //Lecture des parametres initialisation =
     * DParametresNavmer.lectureFichierIni("/home/users/maillot/test/fer.ini"); scoefnav =
     * DParametresNavmer.lectureFichierNav("/home/users/maillot/test/ferryOK.nav"); //Initialisation des Parametres a
     * partir des lectures precedentes paramsNv.parametresINI(initialisation); paramsNv.parametresNAV(scoefnav);
     * //Lancement du calcul nv.calcule(); IResultatsNavmer resultsNv = IResultatsNavmerHelper.narrow(nv.resultats());
     * if (resultsNv == null) System.out.println(" resultsN null..."); sdata[0] = resultsNv.resultatsDAT(); //Deuxieme
     * trajectoire: prise encompte des courants sidxzonescourant =
     * DParametresNavmer.lectureIndexZonesCourant("/home/users/maillot/test/tstc.idx"); szonescourant =
     * DParametresNavmer.lectureFichierCou("/home/users/maillot/test/tstc", sidxzonescourant);
     * paramsNv.parametresIDX(sidxzonescourant); paramsNv.parametresCOU(szonescourant); //Lancement du calcul
     * nv.calcule(); resultsNv = IResultatsNavmerHelper.narrow(nv.resultats()); if (resultsNv == null)
     * System.out.println(" resultsN null..."); sdata[1] = resultsNv.resultatsDAT(); trajectoire pa = new
     * trajectoire(sdata); JFrame f = new JFrame("Trajectoire Ferry"); f.addWindowListener(new WindowAdapter() { public
     * void windowClosing (WindowEvent e) { System.exit(0); } }); f.getContentPane().add(pa, BorderLayout.CENTER);
     * f.setSize(new Dimension(1280, 1024)); f.setVisible(true); // System.exit(0);
     */
  }
}

/**
 * ....
 * 
 * @version $Id: ClientNavmer.java,v 1.2 2006-10-19 14:12:28 deniger Exp $
 * @author $Author: deniger $
 */
class trajectoire extends JPanel {
  SResultatsDAT[] sdata_;

  public trajectoire(SResultatsDAT[] _sdata) {
    sdata_ = _sdata;
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g); // paint background
    g.translate(750, 200);
    for (int j = 0; j < sdata_.length; j++) {
      for (int i = 0; i < sdata_[j].etats.length; i++) {
        if (j == 1) {
          g.setColor(Color.red);
        }
        g.drawRect((int) ((sdata_[j].etats[i].cinematique.x) / 40), (int) ((sdata_[j].etats[i].cinematique.y) / 40), 0,
            0);
      }
    }
    repaint();
  }
}
