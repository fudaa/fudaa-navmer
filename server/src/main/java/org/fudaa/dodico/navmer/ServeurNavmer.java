/*
 * @file         ServeurNavmer.java
 * @creation     2000-02-16
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.navmer;
import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;
/**
 * Une classe serveur pour Navmer.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public final class ServeurNavmer {
  
  private ServeurNavmer (){}
  public static void main(String[] _args) {
    String nom=
      (_args.length > 0
        ? _args[0]
        : CDodico.generateName("::navmer::ICalculNavmer"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cete M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculNavmer.class));
    System.out.println("Navmer server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
