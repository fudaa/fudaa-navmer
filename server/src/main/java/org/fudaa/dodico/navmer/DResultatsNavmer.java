/**
 * @file         DResultatsNavmer.java
 * @creation     2000-09-21
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.navmer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

import org.fudaa.dodico.corba.navmer.*;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fichiers.NativeBinaryInputStream;
/**
 * Lecture du fichier Resultats Navmer.
 *
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class DResultatsNavmer
  extends DResultats
  implements IResultatsNavmer,IResultatsNavmerOperations {
  /**
   * Nom du fichier resultats.
   */
  String fichier_;
  /**
   * Cache de la structure resultats.
   */
  SResultatsDAT sData_;
  public DResultatsNavmer() {
    super();
  }
  public final Object clone() throws CloneNotSupportedException {
    DResultatsNavmer r= new DResultatsNavmer();
    r.setFichier(fichier_);
    return r;
  }
  public SResultatsDAT resultatsDAT() {
    //Choix temporaire: Cache de la structure SData
    if (sData_ == null) {
      sData_= lectureFichierDat(fichier_);
      File ficdat= new File(fichier_);
      if (ficdat.exists()) {
        ficdat.delete();
      } //On efface le fichier resultat
    }
    return sData_; //on renvoie ssi la structure contient qque chose
  }
  /**
   * Affecte la valeur Fichier de DResultatsNavmer object.
   * B.M. 08/02/2005 On peut supposer qu'� partir de ce moment l�, les r�sultats
   * ne sont plus corrects, ils sont r�initialis�s.
   *
   * @param _fichier  La nouvelle valeur Fichier
   */
  public void setFichier(String _fichier) {
    fichier_= _fichier;
    sData_=null;
  }
  public static SResultatsDAT lectureFichierDat(String _fichier) {
    SResultatsDAT sdata= null;
    double d;
    SCinematiqueNavire cinematique;
    SOrdreNavire ord;
    SCommandeNavire com;
    SOrdreRemorqueur[] orem;
    SCommandeRemorqueur[] crem;
    SEnvironnement env;
    try {
      Vector vetat= new Vector();
      FileInputStream flux= new FileInputStream(_fichier);
      NativeBinaryInputStream fluxlecture=
        new NativeBinaryInputStream(flux, System.getProperty("os.arch"));
      while ((fluxlecture.available()) != 0) {
        sdata= new SResultatsDAT();
        cinematique= new SCinematiqueNavire();
        ord= new SOrdreNavire();
        com= new SCommandeNavire();
        orem= new SOrdreRemorqueur[nombreRemorqueurs.value];
        crem= new SCommandeRemorqueur[nombreRemorqueurs.value];
        env= new SEnvironnement();
        //Remplissage des structures necessaires
        //a la construction d'une structure SEtatNavire
        //prog.periodeSortieResultats=fluxlecture.readFloat_64();
        fluxlecture.readFloat64();
        readCinematique(cinematique, fluxlecture);
        //prog.periodePriseEnCompteEtat=fluxlecture.readFloat_64();
        fluxlecture.readFloat64();
        readOrdre(ord, fluxlecture);
        readCom(com, fluxlecture);
        int i;
        for (i= 0; i < nombreRemorqueurs.value; i++) {
          orem[i]= new SOrdreRemorqueur();
          double val= fluxlecture.readFloat64();
          if (val == 0.0) {
            orem[i].position= 0;
            orem[i].angle= 0;
            orem[i].force= 0;
          } else {
            //Il faut decomposer l'entier lu
            String v= (new Double(val)).toString();
            orem[i].position= (new Integer(v.substring(0, 1))).intValue();
            orem[i].angle= (new Integer(v.substring(1, 2))).intValue();
            orem[i].force= (new Integer(v.substring(2))).intValue();
          }
        }
        for (i= 0; i < nombreRemorqueurs.value; i++) {
          crem[i]= new SCommandeRemorqueur();
          double val= fluxlecture.readFloat64();
          if (val == 0.) {
            crem[i].position= 0;
            crem[i].angle= 0;
            crem[i].force= 0;
          } else {
            //Il faut decomposer l'entier lu
            String v= (new Double(val)).toString();
            crem[i].position= (new Integer(v.substring(0, 1))).intValue();
            crem[i].angle= (new Integer(v.substring(1, 2))).intValue();
            crem[i].force= (new Integer(v.substring(2))).intValue();
          }
        }
        readEnv(env, fluxlecture);
        d= fluxlecture.readFloat64();
        env.sens= ((d == 0.0) ? false : true);
        vetat.add(new SEtatNavire(cinematique, ord, com, orem, crem, env));
      }
      if(sdata!=null){
      sdata.etats= new SEtatNavire[vetat.size()];
      //Lecture de la sequence de structure SData
      int i;
      for (i= 0; i < vetat.size(); i++) {
        sdata.etats[i]=
          new SEtatNavire(
            ((SEtatNavire)vetat.elementAt(i)).cinematique,
            ((SEtatNavire)vetat.elementAt(i)).ordre,
            ((SEtatNavire)vetat.elementAt(i)).commande,
            ((SEtatNavire)vetat.elementAt(i)).ordreRemorqueurs,
            ((SEtatNavire)vetat.elementAt(i)).commandeRemorqueurs,
            ((SEtatNavire)vetat.elementAt(i)).environnement);
      }
      }
      fluxlecture.close();
    } catch (IOException ex) {
      System.out.println("Erreur lecture Fichier Dat" + ex);
    }
    return sdata;
  }
  private static void readEnv(SEnvironnement _env, NativeBinaryInputStream _fluxlecture) throws IOException {
    _env.angleIncidenceVent= _fluxlecture.readFloat64();
    _env.moduleVent= _fluxlecture.readFloat64();
    _env.profondeur= _fluxlecture.readFloat64();
    _env.vitesseMoyenneX= _fluxlecture.readFloat64();
    _env.vitesseMoyenneY= _fluxlecture.readFloat64();
    _env.vitesseRotationnelleMoyenne= _fluxlecture.readFloat64();
    _env.distanceBergeBabord= _fluxlecture.readFloat64();
    _env.distanceBergeTribord= _fluxlecture.readFloat64();
  }
  private static void readCom(SCommandeNavire _com, NativeBinaryInputStream _fluxlecture) throws IOException {
    _com.gouvernail= _fluxlecture.readFloat64();
    _com.pasHeliceBabord= _fluxlecture.readFloat64();
    _com.pasHeliceTribord= _fluxlecture.readFloat64();
    _com.nombreToursBabord= _fluxlecture.readFloat64();
    _com.nombreToursTribord= _fluxlecture.readFloat64();
    _com.forcePropulseurAvant= _fluxlecture.readFloat64();
    _com.forcePropulseurArriere= _fluxlecture.readFloat64();
    _com.anglePropulseurAvant= _fluxlecture.readFloat64();
    _com.anglePropulseurArriere= _fluxlecture.readFloat64();
  }
  private static void readOrdre(SOrdreNavire _ord, NativeBinaryInputStream _fluxlecture) throws IOException {
    _ord.gouvernail= _fluxlecture.readFloat64();
    _ord.pasHeliceBabord= _fluxlecture.readFloat64();
    _ord.pasHeliceTribord= _fluxlecture.readFloat64();
    _ord.nombreToursBabord= _fluxlecture.readFloat64();
    _ord.nombreToursTribord= _fluxlecture.readFloat64();
    _ord.forcePropulseurAvant= _fluxlecture.readFloat64();
    _ord.forcePropulseurArriere= _fluxlecture.readFloat64();
    _ord.anglePropulseurAvant= _fluxlecture.readFloat64();
    _ord.anglePropulseurArriere= _fluxlecture.readFloat64();
  }
  private static void readCinematique(SCinematiqueNavire _cinematique, NativeBinaryInputStream _fluxlecture) throws IOException {
    _cinematique.instant= _fluxlecture.readFloat64();
    _cinematique.x= _fluxlecture.readFloat64();
    _cinematique.y= _fluxlecture.readFloat64();
    _cinematique.cap= _fluxlecture.readFloat64();
    _cinematique.vitesseLongitudinale= _fluxlecture.readFloat64();
    _cinematique.vitesseLaterale= _fluxlecture.readFloat64();
    _cinematique.vitesseRotationnelle= _fluxlecture.readFloat64();
  }
}
