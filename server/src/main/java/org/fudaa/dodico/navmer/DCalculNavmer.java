/**
 * @file         DCalculNavmer.java
 * @creation     2000-09-21
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.navmer;
import java.io.File;

import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.navmer.ICalculNavmer;
import org.fudaa.dodico.corba.navmer.ICalculNavmerOperations;
import org.fudaa.dodico.corba.navmer.IParametresNavmer;
import org.fudaa.dodico.corba.navmer.IParametresNavmerHelper;
import org.fudaa.dodico.corba.navmer.IResultatsNavmer;
import org.fudaa.dodico.corba.navmer.IResultatsNavmerHelper;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * Lecture/Ecriture des Parametres de Navmer.
 *
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class DCalculNavmer extends DCalcul implements ICalculNavmer,ICalculNavmerOperations {
  private int numeroSession_;
  private String execPath_;
  private SProgression strOperation_;
  /**
   *  DCalculNavmer.
   */
  public DCalculNavmer() {
    super();
    java.util.Random rand= new java.util.Random();
    numeroSession_= rand.nextInt();
  }
  /**
   * retourne la valeur ExecPath de DCalculNavmer object.
   *
   * @return   La valeur ExecPath
   */
  public String getExecPath() {
    return execPath_;
  }
  public final Object clone() throws CloneNotSupportedException {
    return new DCalculNavmer();
  }
  public String toString() {
    return "DCalculNavmer()";
  }
  public String description() {
    return "Navmer, serveur de calcul de trajectoire de bateaux "
      + super.description();
  }
  public SProgression progression() {
    return strOperation_;
  }
  public void calcul(IConnexion _c) {
    if (!verifieConnexion(_c)) {
      return;
    }
    IParametresNavmer params= IParametresNavmerHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    IResultatsNavmer results= IResultatsNavmerHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    log(_c, "lancement du calcul");
    String os= System.getProperty("os.name");
    String path= cheminServeur();
    try {
      int n= (params.parametresIDX() == null) ? 4 : 5;
      //String path = null; enlever par fred
      String[] param= new String[n];
      //Si pas de chemin defini par l'utilisateur dans les preferences
      if (execPath_ == null) {
        /*
        Remplissage d'un tableau de parametres a partir des parametres
        de la fonction
        Chemin du serveur
        Remarque: File.separator renvoie / ou \ selon le type
        du systeme d'exploitation

            path = new String("serveurs" +
                           File.separator + "dodico_serveurs" +
                           File.separator + "navmer" + File.separator);
        */
        // enlever par fred
        //path = "serveurs" + File.separator + "navmer" + File.separator;
        //On recherche le type de l'OS
        String nomexec;
        //Definition des nom des executables a lancer selon l'OS
        if (os.startsWith("Win")) {
          nomexec= "navmer-Win95.exe";
        } else {
          nomexec= "navmer-Linux.x";
        }
        param[0]= path + nomexec;
      } else {
        param[0]= execPath_;
        path= (new File(param[0])).getAbsolutePath();
      }
      //fichier de description du navire
      param[1]= path + "N" + numeroSession_ + ".nav";
      //fichier ini:ordres
      param[2]= path + "I" + numeroSession_ + ".ini";
      //fichier resultat:structures etat
      param[3]= path + "D" + numeroSession_ + ".dat";
      //Si prise en compte des courants
      if (n == 5) {
        System.out.println("--Prise en compte des courants--");
        //fichier parametre:index zones courant
        param[4]= path + "C" + numeroSession_;
      }
      //setProgression("Ecriture du fichier navire",2);
      //Ecriture  des fichiers parametres
      DParametresNavmer.ecritureFichierNav(param[1], params.parametresNAV());
      //setProgression("Ecriture du fichier d'ordres",4);
      DParametresNavmer.ecritureFichierIni(param[2], params.parametresINI());
      //Ecriture des fichiers courant
      if (n == 5) {
        //setProgression("Ecriture du fichier de courants",10);
        DParametresNavmer.ecritureIndexZonesCourant(
          param[4],
          params.parametresIDX());
        DParametresNavmer.ecritureFichierCou(param[4], params.parametresCOU());
      }
      //lancement calcul
      CExec exec= new CExec(param);
      //Permet de rediriger la sortie du programme appelle
      //sur la sortie standard
      exec.setOutStream(System.out);
      //setProgression("Calcul en cour...",50);
      System.out.println("--Debut du calcul--");
      //Lancement de l'execution
      exec.exec();
      System.out.println("--Fin du calcul--");
      //setProgression("Fin du calcul",100);
      //setProgression(null,0);
      //enlever par fred
      //results_ = new CResultatsNavmer(param[3]);
      results.setFichier(param[3]);
      File ficnav= new File(param[1]);
      File ficini= new File(param[2]);
      //Effacement des fichiers parametres apres utilisation
      //Le fichier de resultat est efface dans la classe resultat
      if (ficnav.exists()) {
        ficnav.delete();
      }
      if (ficini.exists()) {
        ficini.delete();
      }
      if (n == 5) {
        //Effacement des fichiers specifiques aux courants
        File ficidx= new File(param[4]);
        if (ficidx.exists()) {
          ficidx.delete();
        }
        for (int i= 0; i < (params.parametresIDX().zonesCourant).length; i++) {
          File ficcou= new File(new String(param[4] + i + ".cou"));
          if (ficcou.exists()) {
            ficcou.delete();
          }
        }
      }
      log(_c, "calcul termin�");
    } catch (Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
  /**
   * Affecte la valeur ExecPath de DCalculNavmer object.
   *
   * @param _execPath  La nouvelle valeur ExecPath
   */
  public void setExecPath(String _execPath) {
    execPath_= _execPath;
  }
}
