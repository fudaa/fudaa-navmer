/**
 * @file         DParametresNavmer.java
 * @creation     2000-09-21
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.navmer;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StreamTokenizer;
import java.util.Vector;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.navmer.*;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.fichiers.NativeBinaryInputStream;
import org.fudaa.dodico.fichiers.NativeBinaryOutputStream;
/**
 * Lecture/Ecriture des Parametres de Navmer.
 *
 * @version      $Revision: 1.15 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class DParametresNavmer
  extends DParametres
  implements IParametresNavmer,IParametresNavmerOperations {
  /**
   * fichier ini.
   */
  private SParametresINI initialisation_;
  /**
   * fichier nav.
   */
  private SCoefficientsNavire sCoefNav_;
  /**
   * fichier idx.
   */
  private SIndexZonesCourant indexZonesCourant_;
  /**
   * fichier cou.
   */
  private SZoneCourant[] zonesCourant_;
  /**
   * DParametresNavmer.
   */
  public DParametresNavmer() {
    super();
  }
  public final Object clone() throws CloneNotSupportedException {
    return new DParametresNavmer();
  }
  public String toString() {
    String s= "DParametresNavmer";
    return s;
  }
  public SParametresINI parametresINI() {
    return initialisation_;
  }
  public void parametresINI(SParametresINI _sIni) {
    initialisation_= _sIni;
  }
  public SCoefficientsNavire parametresNAV() {
    return sCoefNav_;
  }
  public void parametresNAV(SCoefficientsNavire _sCoefNav) {
    sCoefNav_= _sCoefNav;
  }
  public SIndexZonesCourant parametresIDX() {
    return indexZonesCourant_;
  }
  public void parametresIDX(SIndexZonesCourant _indexZonesCourant) {
    indexZonesCourant_= _indexZonesCourant;
  }
  public SZoneCourant[] parametresCOU() {
    return zonesCourant_;
  }
  public void parametresCOU(SZoneCourant[] _zonescourant) {
    zonesCourant_= _zonescourant;
  }
  /**
   * Cette methode est appellee a chaque fin de ligne.
   * Elle envoie une exception si le caractere de fin de ligne n'est pas
   * elle permet donc de s'assurer de la validite du format de fichier
   *
   * @param _st
   * @return test
   */
  private static int testCharEol(StreamTokenizer _st) {
    try {
      int t= _st.nextToken();
      if (t == '\n' || t == '\r') {
        return t;
      }
      throw new IOException("Mauvais format de fichier" + t);
    } catch (IOException ex) {
      System.err.println("Erreur testCharEol " + ex);
    }
    return -2;
  }
  public static SCaracteristiquesNavire caracteristiquesNavire(SCoefficientsNavire _sCoefNav) {
    /*Decomposition du type Navire
      cf IDL pour en comprendre la structure
      on covertit la valeur en String pour en extraire les sous-chaines
      On reconvertit ensuite les sous-parties en entier.*/
    String val= Double.toString(_sCoefNav.a[7][5]);
    int nbgouv= (new Integer(val.substring(0, 1))).intValue();
    int nbhel= (new Integer(val.substring(1, 2))).intValue();
    int tphel= (new Integer(val.substring(2, 3))).intValue();
    int confprop= (new Integer(val.substring(3, 4))).intValue();
    int chargenav= (new Integer(val.substring(4, 5))).intValue();
    int typenav= (new Integer(val.substring(5, 6))).intValue();
      return new SCaracteristiquesNavire(_sCoefNav.a[0][1], //longueur
      _sCoefNav.a[0][2], //largeur
      _sCoefNav.a[0][3], //tirant
      _sCoefNav.a[0][4], //vitesse max
      _sCoefNav.a[0][5], //Nombre de tours max
      _sCoefNav.a[10][1], //Pas max
      nbgouv, nbhel, tphel, confprop, chargenav, typenav);
  }
  public static SParametresINI lectureFichierIni(String _fichier) {
    SEnteteINI entete= new SEnteteINI();
    entete.etatInitial= new SEtatNavire();
    entete.etatInitial.cinematique= new SCinematiqueNavire();
    entete.etatInitial.ordre= new SOrdreNavire();
    entete.etatInitial.commande= new SCommandeNavire();
    entete.etatInitial.ordreRemorqueurs=
      new SOrdreRemorqueur[nombreRemorqueurs.value];
    entete.etatInitial.commandeRemorqueurs=
      new SCommandeRemorqueur[nombreRemorqueurs.value];
    entete.etatInitial.environnement= new SEnvironnement();
    entete.parametresProgramme= new SParametresProgramme();
    SParametresINI nitialisation=
      new SParametresINI(entete, new SPassageOrdre[0]);
    int t= 0;
    try {
      //Ouverture d'un flux en lecture sur le fichier _fichier
      BufferedReader fluxlecture= new BufferedReader(new FileReader(_fichier));
      StreamTokenizer st = crateTokenizer(fluxlecture);
      //--fin config
      //--Premiere etape:lecture de 'PeriodeSortieResultats'(s),'temps fin'(s)
      //et de PeriodePriseEnCompteEtats
      st.nextToken();
      entete.parametresProgramme.periodeSortieResultats= st.nval;
      st.nextToken();
      entete.tempsFin= st.nval;
      st.nextToken();
      entete.parametresProgramme.periodePriseEnCompteEtat= st.nval;
      t= testCharEol(st);
      //Si fichier dos, on passe le caractere 13
      if (t == '\r') {
        //On repasse le caractere 10
        t= st.nextToken();
        t= 0;
      }
      //Fin Premiere etape--
      //Deuxieme etape: Lecture de
      //Fis(dg) Xs(m) Ys(m) Us(nds ou %) Vs(nds ou 0) Rs(dg/s ou 0) NAV POR
      st.nextToken();
      entete.etatInitial.cinematique.cap= st.nval;
      st.nextToken();
      entete.etatInitial.cinematique.x= st.nval;
      st.nextToken();
      entete.etatInitial.cinematique.y= st.nval;
      st.nextToken();
      entete.etatInitial.cinematique.vitesseLongitudinale= st.nval;
      st.nextToken();
      entete.etatInitial.cinematique.vitesseLaterale= st.nval;
      st.nextToken();
      entete.etatInitial.cinematique.vitesseRotationnelle= st.nval;
      st.nextToken();
      entete.navire= st.sval;
      st.nextToken();
      entete.port= st.sval;
      t= testCharEol(st);
      //Si fichier dos, on passe le caractere 13
      if (t == '\r') {
        //On repasse le caractere 10
        t= st.nextToken();
        t= 0;
      }
      //Fin deuxieme etape--
      //--Troisieme etape
      st.nextToken();
      entete.tempsDebut= st.nval;
      st.nextToken();
      entete.etatInitial.commande.gouvernail= st.nval;
      st.nextToken();
      entete.etatInitial.commande.nombreToursBabord= st.nval;
      st.nextToken();
      entete.etatInitial.commande.nombreToursTribord= st.nval;
      st.nextToken();
      entete.etatInitial.commande.pasHeliceBabord= st.nval;
      st.nextToken();
      entete.etatInitial.commande.pasHeliceTribord= st.nval;
      st.nextToken();
      entete.etatInitial.commande.anglePropulseurAvant= st.nval;
      st.nextToken();
      entete.etatInitial.commande.forcePropulseurAvant= st.nval;
      st.nextToken();
      entete.etatInitial.commande.anglePropulseurArriere= st.nval;
      st.nextToken();
      entete.etatInitial.commande.forcePropulseurArriere= st.nval;
      t= testCharEol(st);
      //Si fichier dos, on passe le caractere 13
      if (t == '\r') {
        //On repasse le caractere 10
        t= st.nextToken();
        t= 0;
      }
      //--Fin troisieme etape
      //Quatrieme Etape: Lecture de
      //Tdeb Gouv Nb Nt PasB PasT Apav Fpav Apar Fpar
      st.nextToken(); //Tdeb deja lu
      st.nextToken();
      entete.etatInitial.ordre.gouvernail= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.nombreToursBabord= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.nombreToursTribord= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.pasHeliceBabord= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.pasHeliceTribord= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.anglePropulseurAvant= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.forcePropulseurAvant= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.anglePropulseurArriere= st.nval;
      st.nextToken();
      entete.etatInitial.ordre.forcePropulseurArriere= st.nval;
      t= testCharEol(st);
      //Si fichier dos, on passe le caractere 13
      if (t == '\r') {
        //On repasse le caractere 10
        t= st.nextToken();
        t= 0;
      }
      //--Fin Quatrieme etape
      //--Debut Cinquieme etape
      //Lecture Prof Avent Fvent Vcxm Vcym Rcm Ybab Ytrib Sens
      st.nextToken();
      entete.etatInitial.environnement.profondeur= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.angleIncidenceVent= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.moduleVent= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.vitesseMoyenneX= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.vitesseMoyenneY= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.vitesseRotationnelleMoyenne= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.distanceBergeBabord= st.nval;
      st.nextToken();
      entete.etatInitial.environnement.distanceBergeTribord= st.nval;
      st.nextToken();
      if (st.nval == 0) {
        entete.etatInitial.environnement.sens= false;
      } else {
        entete.etatInitial.environnement.sens= false;
      }
      t= testCharEol(st);
      //Si fichier dos, on passe le caractere 13
      if (t == '\r') {
        //On repasse le caractere 10
        t= st.nextToken();
        t= 0;
      }
      //Fin Cinquieme etape
      //Debut Sixieme etape
      int j;
      String val= null;
      //Lecture des ordres remorqueurs
      for (j= 0; j < nombreRemorqueurs.value; j++) {
        //Decomposer le nombre lu
        st.nextToken();
        val= Double.toString(st.nval);
        if ("0.0".equals(val)) {
          entete.etatInitial.ordreRemorqueurs[j]= new SOrdreRemorqueur(0, 0, 0);
        } else {
          entete.etatInitial.ordreRemorqueurs[j]=
            new SOrdreRemorqueur(
              (new Double(val.substring(0, 1))).intValue(),
              (new Double(val.substring(1, 2))).intValue(),
              (new Double(val.substring(2))).intValue());
        }
      }
      for (j= 0; j < nombreRemorqueurs.value; j++) {
        //Lecture des commandes remorqueur
        entete.etatInitial.commandeRemorqueurs[j]= new SCommandeRemorqueur();
        st.nextToken();
        val= Double.toString(st.nval);
        if ("0.0".equals(val)) {
          entete.etatInitial.ordreRemorqueurs[j]= new SOrdreRemorqueur(0, 0, 0);
        } else {
          entete.etatInitial.commandeRemorqueurs[j]=
            new SCommandeRemorqueur(
              (new Double(val.substring(0, 1))).intValue(),
              (new Double(val.substring(1, 2))).intValue(),
              (new Double(val.substring(2))).intValue());
        }
        /*On a recuper� les 3 composantes d'un ordre remorqueur cod�es
          dans un seul entier de 5 chiffres:
          1er chiffre:position, 2eme chiffre:angle, 3 derniers:force*/
      }
      t= testCharEol(st);
      //Si fichier dos, on passe le caractere 13
      if (t == '\r') {
        //On repasse le caractere 10
        t= st.nextToken();
        t= 0;
      }
      //Fin Sixieme etape
      //Lecture sequence Ordres
      Vector vordres= new Vector();
      st.nextToken();
      do {
        //On remplit les champs d' un objet ordre
        double instant= st.nval;
        st.nextToken();
        int type= (int)st.nval;
        st.nextToken();
        double valeur= st.nval;
        vordres.addElement(new SPassageOrdre(instant, type, valeur));
        //Si fichier dos, on passe le caractere 13
        t= testCharEol(st);
        if (t == '\r') {
          //On repasse le caractere 10
          t= st.nextToken();
          t= 0;
        }
      } while (st.nextToken() != StreamTokenizer.TT_EOF);
      nitialisation.ordres= new SPassageOrdre[vordres.size()];
      for (int i= 0; i < vordres.size(); i++) {
        nitialisation.ordres[i]=
          new SPassageOrdre(
            ((SPassageOrdre) (vordres.elementAt(i))).instant,
            ((SPassageOrdre)vordres.elementAt(i)).type,
            ((SPassageOrdre)vordres.elementAt(i)).valeur);
      }
      //Il est normalement pas necessaire de fermer le FileReader
      fluxlecture.close();
    } catch (Exception ex) {
      System.err.println("Erreur au moment de la lecture du fichier ini:" + ex);
    }
    return nitialisation;
  }
  /**
   *
   * @param _fichier
   * @param _initialisation
   */
  public static boolean ecritureFichierIni(
    String _fichier,
    SParametresINI _initialisation) {
    try {
      PrintWriter fluxecriture= new PrintWriter(new FileWriter(_fichier));
      //Lecture de la structure _Initialisation
      SEnteteINI entete= _initialisation.entete;
      SParametresProgramme paramprog=
        _initialisation.entete.parametresProgramme;
      SCinematiqueNavire paramcin=
        _initialisation.entete.etatInitial.cinematique;
      SCommandeNavire paramcom= _initialisation.entete.etatInitial.commande;
      SOrdreNavire paramordre= _initialisation.entete.etatInitial.ordre;
      SOrdreRemorqueur[] ordrerem=
        _initialisation.entete.etatInitial.ordreRemorqueurs;
      SCommandeRemorqueur[] commanderem=
        _initialisation.entete.etatInitial.commandeRemorqueurs;
      SEnvironnement paramenv= _initialisation.entete.etatInitial.environnement;
      String vir = CtuluLibString.VIR;
      //On cr�e des string � partir des infos receuillies
      String line1=
        paramprog.periodeSortieResultats
          + vir
          + entete.tempsFin
          + vir
          + paramprog.periodePriseEnCompteEtat
          + vir;
      String line2=
        paramcin.cap
          + vir
          + paramcin.x
          + vir
          + paramcin.y
          + vir
          + paramcin.vitesseLongitudinale
          + vir
          + paramcin.vitesseLaterale
          + vir
          + paramcin.vitesseRotationnelle
          + vir
          + _initialisation.entete.navire
          + vir
          + _initialisation.entete.port
          + vir;
      String line3=
        1
          + vir
          + paramcom.gouvernail
          + vir
          + paramcom.nombreToursBabord
          + vir
          + paramcom.nombreToursTribord
          + vir
          + paramcom.pasHeliceBabord
          + vir
          + paramcom.pasHeliceTribord
          + vir
          + paramcom.anglePropulseurAvant
          + vir
          + paramcom.forcePropulseurAvant
          + vir
          + paramcom.anglePropulseurArriere
          + vir
          + paramcom.forcePropulseurArriere
          + vir;
      String line4=
        1
          + vir
          + paramordre.gouvernail
          + vir
          + paramordre.nombreToursBabord
          + vir
          + paramordre.nombreToursTribord
          + vir
          + paramordre.pasHeliceBabord
          + vir
          + paramordre.pasHeliceTribord
          + vir
          + paramordre.anglePropulseurAvant
          + vir
          + paramordre.forcePropulseurAvant
          + vir
          + paramordre.anglePropulseurArriere
          + vir
          + paramordre.forcePropulseurArriere
          + vir;
      int s= (paramenv.sens ) ? 1 : 0;
      String line5=
        paramenv.profondeur
          + vir
          + paramenv.angleIncidenceVent
          + vir
          + paramenv.moduleVent
          + vir
          + paramenv.vitesseMoyenneX
          + vir
          + paramenv.vitesseMoyenneY
          + vir
          + paramenv.vitesseRotationnelleMoyenne
          + vir
          + paramenv.distanceBergeBabord
          + vir
          + paramenv.distanceBergeTribord
          + vir
          + s
          + vir;
      int i;
      String line6= "";
      boolean zero= true;
      for (i= 0; i < nombreRemorqueurs.value; i++) {
        if (ordrerem[i].position != 0
          || ordrerem[i].angle != 0
          || ordrerem[i].angle != 0) {
          zero= false;
        }
        if (!zero) {
          line6 += ordrerem[i].position
            + ""
            + ordrerem[i].angle
            + ""
            + ordrerem[i].force
            + vir;
        } else {
          line6 += "0,";
        }
        zero= true;
      }
      for (i= 0; i < nombreRemorqueurs.value; i++) {
        if (ordrerem[i].position != 0
          || ordrerem[i].angle != 0
          || ordrerem[i].angle != 0) {
          zero= false;
        }
        if (!zero) {
          line6 += commanderem[i].position
            + ""
            + commanderem[i].angle
            + ""
            + commanderem[i].force
            + vir;
        } else {
          line6 += "0,";
        }
        zero= true;
      }
//      line6 += "\n";
//      fluxecriture.write(line1, 0, line1.length());
//      fluxecriture.write(line2, 0, line2.length());
//      fluxecriture.write(line3, 0, line3.length());
//      fluxecriture.write(line4, 0, line4.length());
//      fluxecriture.write(line5, 0, line5.length());
//      fluxecriture.write(line6, 0, line6.length());
      fluxecriture.println(line1);
      fluxecriture.println(line2);
      fluxecriture.println(line3);
      fluxecriture.println(line4);
      fluxecriture.println(line5);
      fluxecriture.println(line6);
      for (i= 0; i < _initialisation.ordres.length; i++) {
        String tmp=
          new String(
            _initialisation.ordres[i].instant
              + vir
              + _initialisation.ordres[i].type
              + vir
              + _initialisation.ordres[i].valeur
              + vir);
//        fluxecriture.write(tmp, 0, tmp.length());
        fluxecriture.println(tmp);
      }
      fluxecriture.close();
    } catch (Exception ex) {
      System.err.println("Erreur au moment de l'ecriture du .ini:" + ex);
    }
    return true;
  }
  /**
   *
   * @param _fichier
   * @param _sCoefNav
   */
  public static boolean ecritureFichierNav(
    String _fichier,
    SCoefficientsNavire _sCoefNav) {
    try {
      FileOutputStream flux= new FileOutputStream(_fichier);
      NativeBinaryOutputStream fluxecriture=
        new NativeBinaryOutputStream(flux, System.getProperty("os.arch"));
      int i;
      int j;
      for (i= 0; i <= 12; i++) {
        for (j= 0; j <= 9; j++) {
          fluxecriture.writeFloat64((_sCoefNav.a)[i][j]);
        }
      }
      for (i= 0; i <= 72; i++) {
        fluxecriture.writeFloat64((_sCoefNav.poussee)[i]);
      }
      for (i= 0; i <= 18; i++) {
        fluxecriture.writeFloat64((_sCoefNav.ventX)[i]);
      }
      for (i= 0; i <= 18; i++) {
        fluxecriture.writeFloat64((_sCoefNav.ventY)[i]);
      }
      for (i= 0; i <= 18; i++) {
        fluxecriture.writeFloat64((_sCoefNav.ventN)[i]);
      }
      fluxecriture.close();
    } catch (IOException ex) {
      System.err.println("Erreur ecritureFichierNav" + ex);
    }
    return true;
  }
  public static SCoefficientsNavire lectureFichierNav(String _fichier) {
    SCoefficientsNavire scoefn= new SCoefficientsNavire();
    scoefn.a= new double[13][10];
    scoefn.poussee= new double[73];
    scoefn.ventX= new double[19];
    scoefn.ventY= new double[19];
    scoefn.ventN= new double[19];
    try {
      FileInputStream flux= new FileInputStream(_fichier);
      NativeBinaryInputStream fluxlecture=
        new NativeBinaryInputStream(flux, System.getProperty("os.arch"));
      int i;
      int j;
      for (i= 0; i <= 12; i++) {
        for (j= 0; j <= 9; j++) {
          scoefn.a[i][j]= fluxlecture.readFloat64();
        }
      }
      for (i= 0; i <= 72; i++) {
        (scoefn.poussee)[i]= fluxlecture.readFloat64();
      }
      for (i= 0; i <= 18; i++) {
        (scoefn.ventX)[i]= fluxlecture.readFloat64();
      }
      for (i= 0; i <= 18; i++) {
        (scoefn.ventY)[i]= fluxlecture.readFloat64();
      }
      for (i= 0; i <= 18; i++) {
        (scoefn.ventN)[i]= fluxlecture.readFloat64();
      }
      fluxlecture.close();
    } catch (IOException ex) {
      System.err.println("Erreur lectureFichierNav" + ex);
    }
    return scoefn;
  }
  public static SIndexZonesCourant lectureIndexZonesCourant(String _fichier) {
    Vector indexzones= new Vector();
    double xmin;
    double xmax;
    double ymin;
    double ymax;
    int nfic;
    int t;
    try {
      //Ouverture d'un flux en lecture sur le fichier _fichier
      BufferedReader fluxlecture= new BufferedReader(new FileReader(_fichier));
      //--Config. du streamTokenizer
      StreamTokenizer st = crateTokenizer(fluxlecture);
      //--fin config
      st.nextToken();
      do {
        xmin= st.nval;
        st.nextToken();
        xmax= st.nval;
        st.nextToken();
        ymin= st.nval;
        st.nextToken();
        ymax= st.nval;
        st.nextToken();
        nfic= (int)st.nval;
        t= testCharEol(st);
        //Si fichier dos, on passe le caractere 13
        if (t == 13) {
          //On repasse le caractere 10
          t= st.nextToken();
          t= 0;
        }
        indexzones.addElement(
          new SLigneIndexZoneCourant(xmin, xmax, ymin, ymax, nfic));
      } while (st.nextToken() != StreamTokenizer.TT_EOF);
      fluxlecture.close();
    } catch (Exception ex) {
      System.err.println(
        "Erreur au moment de la lecture du fichier indexcourant:" + ex);
    }
    SIndexZonesCourant idxzones= new SIndexZonesCourant();
    idxzones.zonesCourant= new SLigneIndexZoneCourant[indexzones.size()];
    for (int i= 0; i < indexzones.size(); i++) {
      idxzones.zonesCourant[i]=
        new SLigneIndexZoneCourant(
          ((SLigneIndexZoneCourant)indexzones.elementAt(i)).xmin,
          ((SLigneIndexZoneCourant)indexzones.elementAt(i)).xmax,
          ((SLigneIndexZoneCourant)indexzones.elementAt(i)).ymin,
          ((SLigneIndexZoneCourant)indexzones.elementAt(i)).ymax,
          ((SLigneIndexZoneCourant)indexzones.elementAt(i)).numeroZone);
    }
    return idxzones;
  }
  private static StreamTokenizer crateTokenizer(BufferedReader _fluxlecture) {
    StreamTokenizer st= new StreamTokenizer(_fluxlecture);
    st.resetSyntax();
    st.eolIsSignificant(false);
    st.slashSlashComments(false);
    st.slashStarComments(false);
    st.wordChars('a', 'z');
    st.wordChars('A', 'Z');
    st.wordChars('0', '9');
    st.wordChars('$', '$');
    st.parseNumbers();
    st.wordChars(129, 255);
    st.whitespaceChars(',', ',');
    st.whitespaceChars(' ', ' ');
    st.whitespaceChars('\t', '\t');
    return st;
  }
  public static boolean ecritureIndexZonesCourant(
    String _fichier,
    SIndexZonesCourant _idxzones) {
    String tmp;
    try {
      PrintWriter fluxecriture= new PrintWriter(new FileWriter(_fichier));
      for (int i= 0; i < _idxzones.zonesCourant.length; i++) {
        tmp=
          new String(
            (_idxzones.zonesCourant[i]).xmin
              + CtuluLibString.VIR
              + (_idxzones.zonesCourant[i]).xmax
              + CtuluLibString.VIR
              + (_idxzones.zonesCourant[i]).ymin
              + CtuluLibString.VIR
              + (_idxzones.zonesCourant[i]).ymax
              + CtuluLibString.VIR
              + (_idxzones.zonesCourant[i]).numeroZone
              + CtuluLibString.VIR);
//        fluxecriture.write(tmp, 0, tmp.length());
        fluxecriture.println(tmp);
      }
      fluxecriture.close();
    } catch (Exception ex) {
      System.err.println("Erreur ecriture indexzonescourants:" + ex);
    }
    return true;
  }
  public static SZoneCourant lectureZoneCourant(String _fichier) {
    Vector cinematique= new Vector();
    double xc;
    double yc;
    double prof;
    double ybab;
    double ytrib;
    double vcx;
    double vcy;
    int t;
    try {
      //Ouverture d'un flux en lecture sur le fichier _fichier
      BufferedReader fluxlecture=
        new BufferedReader(new FileReader(new String(_fichier)));
      StreamTokenizer st = crateTokenizer(fluxlecture);
      //--fin config
      //Lecture sur chaque ligne des elements de la structure
      //SCinematiqueCourant:xc,yc,prof,ybab,ytrib,vcx,vcy.
      st.nextToken();
      do {
        xc= st.nval;
        st.nextToken();
        yc= st.nval;
        st.nextToken();
        prof= st.nval;
        st.nextToken();
        ybab= st.nval;
        st.nextToken();
        ytrib= st.nval;
        st.nextToken();
        vcx= st.nval;
        st.nextToken();
        vcy= st.nval;
        //Les deux derni�res colonnes sont systematiquement �gales � 0
        //Il est donc inutile de stocker leur valeur
        st.nextToken();
        st.nextToken();
        t= testCharEol(st);
        //Si fichier dos, on passe le caractere 13
        if (t == '\r') {
          //On repasse le caractere 10
          t= st.nextToken();
          t= 0;
        }
        cinematique.addElement(
          new SCinematiqueCourant(xc, yc, prof, ybab, ytrib, vcx, vcy));
      } while (st.nextToken() != StreamTokenizer.TT_EOF);
      fluxlecture.close();
    } catch (Exception ex) {
      System.err.println(
        "Erreur au moment de la lecture du fichier de courants:" + ex);
    }
    SZoneCourant zone= new SZoneCourant();
    zone.cinematiques= new SCinematiqueCourant[cinematique.size()];
    for (int i= 0; i < cinematique.size(); i++) {
      zone.cinematiques[i]=
        new SCinematiqueCourant(
          ((SCinematiqueCourant)cinematique.elementAt(i)).x,
          ((SCinematiqueCourant)cinematique.elementAt(i)).y,
          ((SCinematiqueCourant)cinematique.elementAt(i)).profondeur,
          ((SCinematiqueCourant)cinematique.elementAt(i)).distanceBergeBabord,
          ((SCinematiqueCourant)cinematique.elementAt(i)).distanceBergeTribord,
          ((SCinematiqueCourant)cinematique.elementAt(i)).vitesseX,
          ((SCinematiqueCourant)cinematique.elementAt(i)).vitesseY);
    }
    return zone;
  }
  public static SZoneCourant[] lectureFichierCou(
    String _basenom,
    SIndexZonesCourant _idxzones) {
    SZoneCourant[] tabzones= new SZoneCourant[(_idxzones.zonesCourant).length];
    try {
      //Boucle sur chacun des fichiers courant
      for (int i= 0; i < (_idxzones.zonesCourant).length; i++) {
        tabzones[i]= lectureZoneCourant(_basenom + i + ".cou");
      }
    } catch (Exception ex) {}
    return tabzones;
  }
  public static boolean ecritureZoneCourant(
    String _fichier,
    SZoneCourant _zone) {
    String tmp;
    try {
      PrintWriter fluxecriture=
        new PrintWriter(new FileWriter(new String(_fichier)));
      for (int i= 0; i < _zone.cinematiques.length; i++) {
        tmp=
          new String(
            (_zone.cinematiques[i]).x
              + CtuluLibString.VIR
              + (_zone.cinematiques[i]).y
              + CtuluLibString.VIR
              + (_zone.cinematiques[i]).profondeur
              + CtuluLibString.VIR
              + (_zone.cinematiques[i]).distanceBergeBabord
              + CtuluLibString.VIR
              + (_zone.cinematiques[i]).distanceBergeTribord
              + CtuluLibString.VIR
              + (_zone.cinematiques[i]).vitesseX
              + CtuluLibString.VIR
              + (_zone.cinematiques[i]).vitesseY
              + CtuluLibString.VIR
              + "0.0 , 0.0");
        // Les deux derni�res sont = � 0
        fluxecriture.println(tmp);
//        fluxecriture.write(tmp, 0, tmp.length());
      }
      fluxecriture.close();
    } catch (Exception ex) {
      System.err.println("Erreur ecriture fichier courant:" + ex);
    }
    return true;
  }
  /**
   * Cette fonction doit prendre en parametres un index de fichiers courant
   * afin de savoir Le nombre de fichiers a ecrire.
   *
   * @param _basenom
   * @param _zones
   * @return true
   */
  public static boolean ecritureFichierCou(
    String _basenom,
    SZoneCourant[] _zones) {
    try {
      for (int i= 0; i < _zones.length; i++) {
        ecritureZoneCourant(_basenom + i + ".cou", _zones[i]);
      }
    } catch (Exception ex) {}
    return true;
  }
}
