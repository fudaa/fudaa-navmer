/*
 * @file         NavmerGrapheResultats.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import com.memoire.bu.BuDialogError;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;
/**
 * Permet de creer un graphe avec plusieurs courbes
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
class NavmerGrapheResultats extends BGraphe implements PropertyChangeListener {
  public static final int margeGauche= 60;
  public static final int margeDroite= 150;
  private double[][] valeurs_;
  private double[] temps_;
  private String nomaxeX_;
  private String[] nomcol_;
  private Graphe graphe_;
  private Axe axeX_;
  private CourbeDefault courbe_;
  private final Color[] col=
    { Color.black, Color.blue, Color.green, Color.orange, Color.pink };
  private final Vector axesY= new Vector(); //Vecteur d'axe Y
  public NavmerGrapheResultats(
    final double[][] _valeurs,
    final double[] _temps,
    final String _nomaxeX,
    final String[] _nomcol) {
    graphe_= null;
    axeX_= null;
    valeurs_= _valeurs;
    temps_= _temps;
    nomaxeX_= _nomaxeX;
    nomcol_= _nomcol;
    initGraphe();
  }
  //Cette fonction renvoie un axe bien dimensionn�
  private Axe creeAxe(final double[] _tab, final String _titre) {
    final Axe axe= new Axe();
    axe.titre_= _titre;
    axe.vertical_= true;
    axe.graduations_= true;
    axe.grille_= false;
    //Calcul du min et du max
    double min= _tab[0];
    double max= _tab[0];
    for (int i= 0; i < _tab.length; i++) {
      min= (_tab[i] < min) ? _tab[i] : min;
      max= (_tab[i] > max) ? _tab[i] : max;
    }
    //Calcul du pas � l'aide de min/max
    axe.pas_= (max - min) / 10;
    axe.minimum_= min;
    axe.maximum_= max;
    axe.visible_= true;
    axesY.add(axe);
    return axe;
  }
  public void initGraphe() {
    int i;
    final Marges marges= new Marges();
    graphe_= new Graphe();
    graphe_.animation_= false;
    graphe_.legende_= false;
    graphe_.copyright_= false;
    graphe_.marges_= marges;
    //Determination de l'intervalle de temps(Abscisse)
    //Calcul du pas pour le temps
    //Axe des x
    axeX_= new Axe();
    axeX_.titre_= nomaxeX_;
    axeX_.unite_= "";
    axeX_.vertical_= false;
    axeX_.graduations_= true;
    axeX_.grille_= false;
    axeX_.minimum_= temps_[0];
    axeX_.maximum_= temps_[temps_.length - 1];
    axeX_.pas_= Math.floor((axeX_.maximum_ - axeX_.maximum_) / temps_.length);
    axeX_.visible_= true;
    graphe_.ajoute(axeX_);
    int nbcourbes= valeurs_[0].length;
    //Au plus trois courbes
    if (valeurs_[0].length > col.length) {
      new BuDialogError(
        null,
        null,
        "Trop de courbes � afficher"
          + "seules les 3 premi�re seront affich�es.")
        .activate();
      nbcourbes= 3;
    }
    //Definition de la marge en fonction du nombre de courbes
    marges.gauche_= margeGauche * nbcourbes;
    marges.droite_= margeDroite;
    marges.haut_= 45;
    marges.bas_= 30;
    for (int u= 0; u < nbcourbes; u++) {
      final double[] vals= new double[valeurs_.length];
      Axe axe= new Axe();
      courbe_= new CourbeDefault();
      courbe_.titre_= nomcol_[u];
      courbe_.trace_= "lineaire";
      courbe_.marqueurs_= false;
      //Creation d'un aspect
      //On affecte une couleur prise dans
      //le tableau membre de la classe
      final Aspect asp= new Aspect();
      asp.contour_= col[u];
      asp.surface_= col[u];
      courbe_.aspect_= asp;
      final Vector valcourbe= new Vector();
      for (i= 0; i < valeurs_.length; i++) {
        final Valeur v= new Valeur();
        vals[i]= v.v_= valeurs_[i][u];
        v.s_= temps_[i];
        valcourbe.add(v);
      }
      // axe = createAxe(vals, nomcol_[u]);
      axe= creeAxe(vals, "");
      axe.aspect_= asp;
      courbe_.valeurs_= valcourbe;
      courbe_.axe_= u;
      courbe_.visible_= true;
      graphe_.legende_= true;
      graphe_.ajoute(axe);
      graphe_.ajoute(courbe_);
    }
    setInteractif(true);
    setGraphe(graphe_);
    fullRepaint();
  }
  public void propertyChange(final PropertyChangeEvent _evt) {
    fullRepaint();
  }
  public Axe getAxeX() {
    return axeX_;
  }
  public void axeXEstVisible(final boolean _b) {
    axeX_.visible_= _b;
  }
  public void graduationsXEstVisible(final boolean _b) {
    axeX_.graduations_= _b;
  }
  public Axe getAxeY(final int numeroAxe) {
    return (Axe)axesY.elementAt(numeroAxe);
  }
}
