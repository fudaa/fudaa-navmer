/*
 * @file         AgIni.java
 * @creation     2000-10-09
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer.these;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.corba.navmer.SPassageOrdre;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * @version $Id: AgIni.java,v 1.8 2006-09-19 15:11:55 deniger Exp $
 * @author Guillaume Desnoix
 */
public final class AgIni {

  private AgIni() {}

  public static SPassageOrdre[] genereOrdres(final GrPolyligne _p) {
    final List r = new ArrayList(100);
    double temps = 0;
    int num = 0;
    double val = 0.;
    temps += 300;
    num = 1;
    val = 30.;
    r.add(new SPassageOrdre(temps, num, val));
    temps += 20;
    num = 2;
    val = 30.;
    r.add(new SPassageOrdre(temps, num, val));
    temps += 300;
    num = 3;
    val = 30.;
    r.add(new SPassageOrdre(temps, num, val));
    temps += 20;
    num = 4;
    val = 30.;
    r.add(new SPassageOrdre(temps, num, val));
    temps += 300;
    num = 5;
    val = 0.5;
    r.add(new SPassageOrdre(temps, num, val));
    temps += 20;
    num = 6;
    val = 0.5;
    r.add(new SPassageOrdre(temps, num, val));
    /*
     * num=0; val=0.; int n=_p.nombre()-1; for(int i=2;i<n;i++) { temps+=(int)(10.*_p.segment(i).longueur());
     * val=_p.angle(i)-_p.angle(i-1); r.add(new SPassageOrdre(temps,num,val)); }
     */
    return (SPassageOrdre[]) r.toArray(new SPassageOrdre[0]);
  }

  public static GrPolyligne creeRoute(final GrPoint _actuel, final GrPoint _objectif, final GrPolyligne _port) {
    final GrPolyligne r = new GrPolyligne();
    r.sommets_.ajoute(_actuel);
    GrPoint a = _actuel;
    final double du = 15.;
    final double dv = 2.5;
    final int nb = 31;
    double l = a.distanceXY(_objectif);
    double lmax = l + 1.;
    while (l < lmax) {
      lmax = l;
      final GrVecteur u = _objectif.soustraction(a).normaliseXY();
      u.z_ = 0.;
      final GrVecteur v = u.rotationZ90();
      final GrPoint[] p = new GrPoint[nb];
      for (int i = 0; i < nb; i++) {
        final int j = i - nb / 2; // (i+1)/2*(i%2==0 ? 1 : -1);
        p[i] = a.addition(v.multiplication(dv * j)).addition(u.multiplication(du));
      }
      final GrPoint m = meilleurXY(p, _port, _objectif);
      a = m;
      r.sommets_.ajoute(a);
      l = a.distanceXY(_objectif);
    }
    r.sommets_.ajoute(_objectif);
    return r;
  }

  /*
   * public static GrPolyligne creeRoute (GrPoint _actuel,GrPoint _objectif,GrPolyligne _port) { return
   * (_actuel.y>=_objectif.y) ? creeRouteYD(_actuel,_objectif,_port) : creeRouteYC(_actuel,_objectif,_port); } public
   * static GrPolyligne creeRouteYD (GrPoint _actuel,GrPoint _objectif,GrPolyligne _port) { GrPolyligne r=new
   * GrPolyligne(); r.sommets.ajoute(_actuel); final double DY=15.; final double DX=4.; final int NB=21; GrPoint[] p=new
   * GrPoint[NB]; double x=_actuel.x; for(double y=_actuel.y-DY; y-DY>_objectif.y; y-=DY) { for(int i=0;i<NB;i++)
   * p[i]=new GrPoint(x-DX*(NB/2)+DX*i,y,0.); GrPoint m=meilleurXY(p,_port,_objectif); x=m.x; r.sommets.ajoute(m);
   * System.err.println("XY="+(int)x+","+(int)y); } r.sommets.ajoute(_objectif); return r; } public static GrPolyligne
   * creeRouteYC (GrPoint _actuel,GrPoint _objectif,GrPolyligne _port) { GrPolyligne r=new GrPolyligne();
   * r.sommets.ajoute(_actuel); final double DY=15.; final double DX=4.; final int NB=21; GrPoint[] p=new GrPoint[NB];
   * double x=_actuel.x; for(double y=_actuel.y+DY; y+DY<_objectif.y; y+=DY) { for(int i=0;i<NB;i++) p[i]=new
   * GrPoint(x-DX*(NB/2)+DX*i,y,0.); GrPoint m=meilleurXY(p,_port,_objectif); x=m.x; r.sommets.ajoute(m);
   * System.err.println("XY="+(int)x+","+(int)y); } r.sommets.ajoute(_objectif); return r; }
   */
  public static GrPolyligne lisse(final GrPolyligne _p) {
    final GrPolyligne r = new GrPolyligne();
    r.sommets_.ajoute(_p.sommet(0));
    final int n = _p.nombre();
    for (int i = 1; i < n - 1; i++) {
      final double x = (_p.sommet(i - 1).x_ + _p.sommet(i).x_ + _p.sommet(i + 1).x_) / 3.;
      final double y = (_p.sommet(i - 1).y_ + _p.sommet(i).y_ + _p.sommet(i + 1).y_) / 3.;
      final GrPoint q = new GrPoint(x, y, 0.);
      r.sommets_.ajoute(q);
    }
    r.sommets_.ajoute(_p.sommet(n - 1));
    return r;
  }

  private  static GrPoint meilleurXY(final GrPoint[] p, final GrPolyligne q, final GrPoint o) {
    GrPoint r = null;
    double lmin = Double.MAX_VALUE;
    final int ni = p.length;
    for (int i = 0; i < ni; i++) {
      final GrPoint s = q.pointPlusProcheXY(p[i]);
      if (s.distanceXY(p[i]) >= 20.) // secure
      {
        final double l = o.distanceXY(p[i]);
        if (l <= lmin) {
          lmin = l;
          r = p[i];
        }
      }
    }
    if (r == null) {
      r = p[ni / 2];
    }
    return r;
  }
}
