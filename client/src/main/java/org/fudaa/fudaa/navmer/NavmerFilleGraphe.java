/*
 * @file         NavmerFilleGraphe.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPrintable;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuToggleButton;

import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.BGraphePersonnaliseur;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class NavmerFilleGraphe
  extends BuInternalFrame
  implements BuPrintable, InternalFrameListener {
  // d�claration de variable priv�e
  private BGraphe graphe_= null;
  private NavmerImplementation imp_= null;
  private BuToggleButton btPerso_= null;
  private BuInternalFrame fperso_= null;
  private JComponent[] tools_= null;
  public NavmerFilleGraphe(final BuCommonInterface _appli, final String _titre) {
    super(_titre, true, true, true, true);
    imp_= (NavmerImplementation)_appli.getImplementation();
    setBackground(Color.white);
    btPerso_= new BuToggleButton("Personnalisation du graphique");
    btPerso_.setActionCommand("BOUTON_PERSO");
    btPerso_.addActionListener(this);
    //SpecificTools
    tools_= new JComponent[1];
    tools_[0]= btPerso_;
    graphe_= new BGraphe();
    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(graphe_, BorderLayout.CENTER);
    setPreferredSize(new Dimension(600, 600));
    pack();
    addInternalFrameListener(this);
    imp_.addInternalFrame(this);
  }
  /**
   * Accesseur au graphe
   */
  public BGraphe getGraphe() {
    return graphe_;
  }
  /**
   * Mutateur du graphe
   */
  public void setGraphe(final BGraphe _graphe) {
    getContentPane().removeAll();
    graphe_= _graphe;
    getContentPane().add(graphe_, BorderLayout.CENTER);
  }
  public JComponent[] getSpecificTools() {
    return tools_;
  }
  public void actionPerformed(final ActionEvent _evt) {
    //Affichage du Personnaliseur de graphe
    if ("BOUTON_PERSO".equals(_evt.getActionCommand())) {
      if (btPerso_.isSelected()) {
        final BGraphePersonnaliseur gperso=
          new BGraphePersonnaliseur(graphe_.getGraphe());
        //ecouteur des evenements de mise � jour du graphe
        gperso.addPropertyChangeListener((NavmerGrapheResultats)graphe_);
        //InternaFrame contenant le graphe_
        fperso_=
          new BuInternalFrame("Personnaliseur", false, false, false, false);
        fperso_.setContentPane(gperso);
        imp_.getImplementation().addInternalFrame(fperso_);
      } else {
        if (fperso_ != null) {
          fperso_.setVisible(false);
          imp_.removeInternalFrame(fperso_);
        }
      }
    }
  }
  /**
   * Autorise l'impression
   */
  public String[] getEnabledActions() {
    final String[] r= new String[] { "IMPRIMER", "PREVISUALISER", };
    return r;
  }
  /**
   * Methode d'impression d'un graphe
   */
  public void print(final PrintJob _job, final Graphics _g) {
    BuPrinter.INFO_DOC= new BuInformationsDocument();
    BuPrinter.INFO_DOC.name= getTitle();
    BuPrinter.INFO_DOC.logo= null;
    BuPrinter.printComponent(_job, _g, graphe_);
  }
  public void internalFrameClosed(final InternalFrameEvent e) {
    setVisible(false);
    imp_.removeInternalFrame(this);
    if (fperso_ != null) {
      fperso_.setVisible(false);
      imp_.removeInternalFrame(fperso_);
    }
  }
  public void internalFrameActivated(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameDeactivated(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameOpened(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameClosing(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameIconified(final InternalFrameEvent e) {}
  /**
   *  Non implante
   */
  public void internalFrameDeiconified(final InternalFrameEvent e) {}
}
