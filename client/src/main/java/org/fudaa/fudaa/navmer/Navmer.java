/**
 * @file         Navmer.java
 * @creation     2000-10-03
 * @modification $Date: 2007-01-19 13:14:37 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;

import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * Classe de lancement de l'application Navmer. Contient la methode main.
 * 
 * @version $Revision: 1.8 $ $Date: 2007-01-19 13:14:37 $ by $Author: deniger $
 * @author Nicolas Maillot
 */
public class Navmer {

  /**
   * put your documentation comment here
   * 
   * @param args
   */
  public static void main(final String[] args) {
    final Fudaa f = new Fudaa();
    f.launch(args, NavmerImplementation.informationsSoftware(), false);
    f.startApp(new NavmerImplementation());
  }
}