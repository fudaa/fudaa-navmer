/*
 * @file         NavmerFilleTableauResultats.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuTable;

import org.fudaa.dodico.corba.navmer.SResultatsDAT;

import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.tableau.EbliFilleTableau;
/**
 * Composant qio contient le tableau des resultats ainsi
 * que les boutons d'acces vers les graphiques
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class NavmerFilleTableauResultats extends EbliFilleTableau {
  NavmerImplementation imp_;
  BuButton btOptions_= null;
  BuButton btGraphique_= null;
  BuButton btGraphiqueSortie_= null;
  BuButton btExport_= null;
  NavmerEtatsTableModel tb_;
  BuTable table_;
  boolean[] tabchoix_; //Cases coch�es
  private JComponent[] tools_= null;
  public NavmerFilleTableauResultats(
    final NavmerImplementation _imp,
    final NavmerEtatsTableModel _tb) {
    super();
    tb_= _tb;
    imp_= _imp;
    setUtiliseFormules(false);
    //Ce bouton va ouvrir une liste d'onglets destin�s � choisir
    //les colonnes � afficher
    btOptions_= new BuButton("Choix des colonnes � afficher");
    btOptions_.setActionCommand("bouton_options");
    btOptions_.addActionListener(this);
    //Ce bouton permet la creation d'un graphique associ� � la zone
    //Selectionn�e
    btGraphique_= new BuButton("Creation d'un graphique");
    btGraphique_.setActionCommand("bouton_graphique");
    btGraphique_.addActionListener(this);
    //Bouton qui cree un graphique de sortie superpose
    btGraphiqueSortie_= new BuButton("Graphiques superposes");
    btGraphiqueSortie_.setActionCommand("bouton_graphique_sup");
    btGraphiqueSortie_.addActionListener(this);
    //Bouton qui exporte les resultats en fichier texte
    btExport_= new BuButton("Export");
    btExport_.setActionCommand("bouton_export");
    btExport_.addActionListener(this);
    tools_= new JComponent[4];
    //Ce bouton active l'ouverture de l'editeur de la table
    tools_[0]= btOptions_;
    //Creation d'un graphique � partir de la selection
    tools_[1]= btGraphique_;
    tools_[2]= btGraphiqueSortie_;
    tools_[3]= btExport_;
    table_= new BuTable(_tb);
    table_.setColumnSelectionAllowed(true);
    table_.setRowSelectionAllowed(false);
    (table_.getTableHeader()).setReorderingAllowed(false);
    setTable(table_);
    setPreferredSize(new Dimension(600, 500));
    setSize(getPreferredSize());
    imp_.addInternalFrame(this);
  }
  public void actionPerformed(final ActionEvent _evt) {
    // On charge le tableau des checkbox coches
    tabchoix_= (boolean[])imp_.projet_.getParam("colonnesres");
    if ("bouton_options".equals(_evt.getActionCommand())) {
      final NavmerEditionResultats dial=
        new NavmerEditionResultats(imp_.getApp(), null, tabchoix_);
      if (dial.activate() != JOptionPane.CANCEL_OPTION) {
        tabchoix_= dial.getResultats();
        imp_.projet_.addParam("colonnesres", tabchoix_);
        getTable().setModel(
          tb_=
            new NavmerEtatsTableModel(
              (SResultatsDAT)imp_.projet_.getResult("ficdat"),
              tabchoix_));
      }
    }
    double[][] dvalues= null; //Donnees selectionnees
    String[] nomcol= null; //nom des colonnes
    SResultatsDAT dt= null;
    Object[][] ovalues= null;
    //On recupere les donn�es si une demande de graphique a �t� faite
    if ("bouton_graphique".equals(_evt.getActionCommand())
      || "bouton_graphique_sup".equals(_evt.getActionCommand())) {
      ovalues= table_.getSelectedValues();
      //Il faut tester si une zone a bien �t� selectionn�e
      if (ovalues[0].length > 0) {
        //Tableau selectionn� sous forme object puis Double
        dvalues= new double[ovalues.length][ovalues[0].length];
        for (int i= 0; i < ovalues.length; i++) {
          for (int j= 0; j < ovalues[i].length; j++) {
            dvalues[i][j]= ((Double)ovalues[i][j]).doubleValue();
          }
        }
        //On recup�re le titre des colonnes
        nomcol= table_.getSelectedColumnNames();
        //enfin la structure resultats
        dt= (SResultatsDAT)imp_.projet_.getResult("ficdat");
      }
    }
    //Creation d'un graphique associ� � la zone selectionn�e
    if ("bouton_graphique".equals(_evt.getActionCommand())) {
      if (ovalues[0].length > 0) {
        //On recup�re le temps
        final double[] temps= new double[tb_.getRowCount()];
        for (int v= 0; v < tb_.getRowCount(); v++) {
          temps[v]= (dt.etats[v].cinematique).instant;
        }
        final NavmerGrapheResultats ng=
          new NavmerGrapheResultats(dvalues, temps, "temps(s)", nomcol);
        final NavmerFilleGraphe g=
          new NavmerFilleGraphe(imp_, "R�sultats de la simulation");
        g.setGraphe(ng);
      } else {
        new BuDialogError(
          null,
          null,
          "Veuillez choisir une ou plusieurs colonnes du tableau.")
          .activate();
      }
    }
    if ("bouton_graphique_sup".equals(_evt.getActionCommand())) {
      int i;
      //Il faut verifier que le calque trajectoire(ou cap)
      //est bien en place
      if (!imp_.getVueCalque().isShowing() ) {
        new BuDialogError(
          null,
          null,
          "La trajectoire ou le cap doivent �tre affich�s � l'�cran.")
          .activate();
      } else if (ovalues[0].length <= 0) {
        new BuDialogError(
          null,
          null,
          "Veuillez choisir une ou plusieurs colonnes du tableau.")
          .activate();
      } else {
        final GrPoint[] pts= new GrPoint[tb_.getRowCount()];
        for (i= 0; i < tb_.getRowCount(); i++) {
          pts[i]=
            new GrPoint(
              dt.etats[i].cinematique.x,
              dt.etats[i].cinematique.y,
              0.);
        }
        //cr�ation du composant de graphiques superpos�s
        new NavmerGraphesSuperposes(imp_, pts, dvalues, nomcol);
      }
    }
    if ("bouton_export".equals(_evt.getActionCommand())) {
      //ouverure d'une boite de dialogue
      final BuFileChooser chooser= new BuFileChooser();
      chooser.setFileFilter(new BuFileFilter("txt", "Fichiers txt"));
      final int returnVal= chooser.showSaveDialog((JFrame)imp_.getApp());
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        //recuperation du nom de fichier saisi
        final String filename= chooser.getSelectedFile().getAbsolutePath();
        //sauvegarde
        tb_.creeFichierTxt(filename);
      }
    }
  }
  public JComponent[] getSpecificTools() {
    return tools_;
  }
  public void print(final PrintJob _job, final Graphics _g) {
    BuPrinter.INFO_DOC= new BuInformationsDocument();
    BuPrinter.INFO_DOC.name= getTitle();
    BuPrinter.INFO_DOC.logo= null;
    BuPrinter.printComponent(_job, _g, getContentPane());
  }
}
