/*
 * @file         NavmerAnalyse.java
 * @creation     2000-10-06
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.Color;

import org.fudaa.ebli.calque.BCalquePoint;
import org.fudaa.ebli.calque.BCalqueSegment;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrSegment;
import org.fudaa.ebli.geometrie.VecteurGrPoint;
import org.fudaa.ebli.geometrie.VecteurGrSegment;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TracePoint;
/**
 * put your module comment here
 * Calque d'affichage des collisions.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public final class NavmerAnalyse {
  private NavmerAnalyse() {}
  
  public static BCalquePoint creeCalqueIntersections(
    final GrPolyligne _port,
    final GrPolyligne _trajectoire) {
    final BCalquePoint calque= new BCalquePoint();
    calque.setName("cqINTERSECTIONS");
    calque.setTitle(NavmerResource.NAVMER.getString("Intersections"));
    calque.setForeground(Color.red);
    calque.setBackground(new Color(255, 192, 192));
    calque.setTypePoint(TracePoint.DISQUE);
    if ((_port != null) && (_trajectoire != null)) {
      final VecteurGrPoint liste= intersectionsXY(_port, _trajectoire);
      calque.setPoints(liste);
    }
    return calque;
  }
  public static BCalqueSegment creeCalqueRisques(
    final GrPolyligne _port,
    final GrPolyligne _trajectoire,
    final double _d) {
    final BCalqueSegment calque= new BCalqueSegment();
    calque.setName("cqRISQUES");
    calque.setTitle(NavmerResource.NAVMER.getString("Risques"));
    calque.setForeground(Color.red);
    if ((_port != null) && (_trajectoire != null)) {
      final VecteurGrSegment liste= risquesXY(_port, _trajectoire, _d);
      calque.setSegments(liste);
    }
    return calque;
  }
  public static BCalqueSegment creeCalqueDangers(
    final GrPolyligne _port,
    final GrPolyligne _trajectoire,
    final double _d) {
    final BCalqueSegment calque= new BCalqueSegment();
    calque.setName("cqDANGERS");
    calque.setTitle(NavmerResource.NAVMER.getString("Dangers"));
    calque.setForeground(Color.red);
    calque.setTypeTrait(TraceLigne.LISSE);
    calque.setEpaisseurTrait(3);
    if ((_port != null) && (_trajectoire != null)) {
      final VecteurGrSegment liste= dangersXY(_port, _trajectoire, _d);
      calque.setSegments(liste);
    }
    return calque;
  }
  // Privees
  private  static VecteurGrPoint intersectionsXY(
    final GrPolyligne p,
    final GrPolyligne q) {
    final VecteurGrPoint liste= new VecteurGrPoint();
    final int ni= p.nombre();
    final int nj= q.nombre();
    for (int i= 0; i < ni - 1; i++) {
      for (int j= 0; j < nj - 1; j++) {
        final GrPoint r= p.segment(i).intersectionXY(q.segment(j));
        if (r != null) {
          liste.ajoute(r);
        }
      }
    }
    return liste;
  }
  private  static VecteurGrSegment risquesXY(
    final GrPolyligne p,
    final GrPolyligne q,
    final double _d) {
    final VecteurGrSegment liste= new VecteurGrSegment();
    final int ni= p.nombre();
    final int nj= q.nombre();
    for (int i= 0; i < ni - 1; i++) {
      for (int j= 0; j < nj - 1; j++) {
        final GrSegment s= p.segment(i).segmentPlusCourtXY(q.segment(j));
        if (s.longueurXY() <= _d) {
          liste.ajoute(s);
        }
      }
    }
    return liste;
  }
  private  static VecteurGrSegment dangersXY(
    final GrPolyligne p,
    final GrPolyligne q,
    final double _d) {
    final VecteurGrSegment liste= new VecteurGrSegment();
    final int ni= p.nombre();
    final int nj= q.nombre();
    for (int i= 0; i < ni - 1; i++) {
      for (int j= 0; j < nj - 1; j++) {
        final GrSegment t= p.segment(i);
        final GrSegment u= q.segment(j);
        final GrSegment r= new GrSegment();
        final GrSegment s= t.segmentPlusCourtXY(u);
        if (s.longueurXY() <= _d) {
          GrPoint a= u.o_;
          for (int k= 0; k < 100; k++) {
            a= u.o_.addition(u.vecteur().multiplication(k / 100.));
            if (t.distanceXY(a) <= _d) {
              break;
            }
          }
          r.o_= a;
          GrPoint b= u.e_;
          for (int k= 100; k >= 0; k--) {
            b= u.o_.addition(u.vecteur().multiplication(k / 100.));
            if (t.distanceXY(b) <= _d) {
              break;
            }
          }
          r.e_= b;
          liste.ajoute(r);
        }
      }
    }
    return liste;
  }
}
