/*
 * @file         NavmerEditionResultats.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
/**
 *
 * @version      $Id: NavmerEditionResultats.java,v 1.7 2006-09-19 15:11:55 deniger Exp $
 * @author       Nicolas Maillot
 */
public class NavmerEditionResultats
  extends BuDialog{
  //Onglets
  private JTabbedPane tpMain_;
  //Bouton de validation des choix
  private BuButton btValidation_;
  private BuButton btAnnulation_;
  private JCheckBox[] tabJcb_;
  String[] nomVariablesEtat_;
  NavmerEditionResultats(
    final BuCommonInterface _parent,
    final BuInformationsSoftware _isoft,
    final boolean[] _tab) {
    super(_parent, _isoft, "");
    setTitle(BuResource.BU.getString("Choix des colonnes � afficher"));
    nomVariablesEtat_= NavmerEtatsTableModel.getNomsColonnesEtat();
    //Remise � jour des CheckBox � partir du tableau _tab
    //si celui � d�j� �t� affect�
    //(si l'utilisateur � d�j� fait un choix).
    if (_tab != null) {
      for (int i= 0; i < nomVariablesEtat_.length; i++) {
        tabJcb_[i].setSelected(_tab[i]);
      }
    }
  }
  /*
   * Cette fonction cr�ee un composant avec tous les checkboxes
   * avec un bouton valider et annuler
   */
  public JComponent getComponent() {
    final String[] nomvariablesetats= NavmerEtatsTableModel.getNomsColonnesEtat();
    //Si l'utilisateur � d�j� fait un choix
    //On recharge ce choix
    final BuPanel pg= new BuPanel();
    pg.setLayout(new BuGridLayout(1));
    int i= 0;
    tabJcb_= new JCheckBox[nomvariablesetats.length];
    /*Creation des boutons annuler et valider*/
    final BuPanel bpButtons= new BuPanel();
    bpButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
    btAnnulation_= new BuButton("Annuler");
    btValidation_= new BuButton("Valider");
    btValidation_.setActionCommand("VALIDER");
    btAnnulation_.setActionCommand("ANNULER");
    btValidation_.addActionListener(this);
    btAnnulation_.addActionListener(this);
    bpButtons.add(btAnnulation_);
    bpButtons.add(btValidation_);
    /*Fin creation des boutons*/
    tpMain_= new JTabbedPane();
    tpMain_.setName("Choix des resultats � afficher");
    tpMain_.setTabPlacement(SwingConstants.LEFT);
    //Onglet cinematique
    final BuPanel cinematique= new BuPanel();
    cinematique.setLayout(new BuGridLayout(2));
    for (i= 0; i < 7; i++) {
      tabJcb_[i]= new JCheckBox(nomvariablesetats[i]);
      cinematique.add(tabJcb_[i]);
    }
    tpMain_.addTab("Cinematique", cinematique);
    //Onglet Ordres
    final BuPanel ordres= new BuPanel();
    ordres.setLayout(new BuGridLayout(2));
    for (i= 7; i < 16; i++) {
      tabJcb_[i]= new JCheckBox(nomvariablesetats[i]);
      ordres.add(tabJcb_[i]);
    }
    tpMain_.addTab("Ordres", ordres);
    //Onglet Commande
    final BuPanel commande= new BuPanel();
    commande.setLayout(new BuGridLayout(2));
    for (i= 16; i < 25; i++) {
      tabJcb_[i]= new JCheckBox(nomvariablesetats[i]);
      commande.add(tabJcb_[i]);
    }
    tpMain_.addTab("Commandes", commande);
    //Onglet environnement
    final BuPanel env= new BuPanel();
    env.setLayout(new BuGridLayout(2));
    for (i= 25; i < 33; i++) {
      tabJcb_[i]= new JCheckBox(nomvariablesetats[i]);
      env.add(tabJcb_[i]);
    }
    tpMain_.addTab("Environnement", env);
    //Onglet remorqueur
    final BuPanel rem= new BuPanel();
    rem.setLayout(new BuGridLayout(2));
    for (i= 33; i < nomvariablesetats.length; i++) {
      tabJcb_[i]= new JCheckBox(nomvariablesetats[i]);
      rem.add(tabJcb_[i]);
    }
    tpMain_.addTab("Remorqueur", rem);
    pg.add(tpMain_);
    pg.add(bpButtons);
    return pg;
  }
  /**
   * Cette fonction regarde l'�tat de chacun de checkbox
   * @return : tableau de booleans dont la taille
   * est �gale au nombre de checkboxes.
   */
  public boolean[] getResultats() {
    final boolean[] tab= new boolean[nomVariablesEtat_.length];
    for (int i= 0; i < nomVariablesEtat_.length; i++) {
      tab[i]= tabJcb_[i].isSelected();
    }
    return tab;
  }
  public void actionPerformed(final ActionEvent _evt) {
    if (_evt.getActionCommand() == "VALIDER") {
      reponse_= JOptionPane.OK_OPTION;
      setVisible(false);
    }
    if (_evt.getActionCommand() == "ANNULER") {
      reponse_= JOptionPane.CANCEL_OPTION;
      setVisible(false);
    }
  }
}
