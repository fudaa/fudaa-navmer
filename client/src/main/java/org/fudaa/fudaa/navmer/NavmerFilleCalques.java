/*
 * @file         NavmerFilleCalques.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPopupButton;

import org.fudaa.ebli.calque.BArbreCalque;
import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.EbliFilleCalques;
import org.fudaa.ebli.controle.BSelecteurResolution;
import org.fudaa.ebli.ressource.EbliResource;

import org.fudaa.fudaa.commun.FudaaLib;
/**
 * D�rive de FudaaFilleCalques en ajoutant les menus de choix
 * de la r�solution et de la densit�
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Nicola Maillot
 */
public class NavmerFilleCalques extends EbliFilleCalques {
  private BuPopupButton pbReglages_;
  public NavmerFilleCalques(final BCalque _gcinit, final BArbreCalque _arbre) {
    super(_gcinit);
    addInternalFrameListener(_arbre);
    final BSelecteurResolution bsResolution= new BSelecteurResolution();
    bsResolution.setPaintLabels(false);
    bsResolution.addPropertyChangeListener(_arbre.getArbreModel());
    final BSelecteurResolution bsDensite= new BSelecteurResolution();
    bsDensite.setPaintLabels(false);
    bsDensite.setPropertyName("densite");
    bsDensite.addPropertyChangeListener(_arbre.getArbreModel());
    final BuPanel srcrgl= new BuPanel();
    srcrgl.setLayout(new BuGridLayout(2, 5, 5));
    srcrgl.setBorder(new EmptyBorder(5, 5, 5, 5));
    srcrgl.add(new JLabel(FudaaLib.getS("R�solution:"), SwingConstants.RIGHT));
    srcrgl.add(bsResolution);
    srcrgl.add(new JLabel(FudaaLib.getS("Densit�:"), SwingConstants.RIGHT));
    srcrgl.add(bsDensite);
    /*
    Dimension ps=srcrgl.getPreferredSize();
    ps.width=Math.min(200,ps.width);
    srcrgl.setPreferredSize(ps);
    */
    pbReglages_= new BuPopupButton(FudaaLib.getS("R�glage"), srcrgl);
    pbReglages_.setToolTipText(
      FudaaLib.getS("R�glage de la densit� et de la r�solution"));
    pbReglages_.setIcon(EbliResource.EBLI.getIcon("resolution.gif"));
  }
  public JComponent[] getSpecificTools() {
    pbReglages_.setDesktop((BuDesktop)getDesktopPane());
    final JComponent[] p= super.getSpecificTools();
    final int l= p.length;
    final JComponent[] r= new JComponent[l + 1];
    System.arraycopy(p, 0, r, 0, l);
    r[l]= pbReglages_;
    return r;
  }
  /*
  public BSelecteurResolution getSelecteurResolution()
  {
    return bsResolution_;
  }

  public BSelecteurResolution getSelecteurDensite()
  {
    return bsDensite_;
  }
  */
}
