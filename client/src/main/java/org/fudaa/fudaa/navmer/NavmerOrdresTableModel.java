/*
 * @file         NavmerOrdresTableModel.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.memoire.fu.FuLib;

import org.fudaa.dodico.corba.navmer.SParametresINI;
/**
 * TableModel du tableau des ordres: 3 colonnes:
 * Temps, ordre, valeur de l'ordre
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class NavmerOrdresTableModel implements TableModel {
  private SParametresINI ini_;
  //Liste des ordres
  public final static String[] ORDRES=
    {
      "Gouvernail",
      "Pas Helice Babord",
      "Pas Helice Tribord",
      "Nombre de Tours Babord",
      "Nombre de Tours Tribord",
      "Force Propulseur Avant",
      "Force Propulseur Arrri�re",
      "Angle Propulseur Avant",
      "Angle Propulseur Arri�re" };
  public NavmerOrdresTableModel(final SParametresINI _ini) {
    ini_= _ini;
  }
  /**
   * @return : nombre de colonnes
   */
  public int getColumnCount() {
    return 3;
  }
  /**
   * @return nombre de lignes
   */
  public int getRowCount() {
    return ini_.ordres.length;
  }
  /**
   * put your documentation comment here
   * @param _row
   * @param _col
   */
  public boolean isCellEditable(final int _row, final int _col) {
    return false;
  }
  public Object getValueAt(final int _row, final int _col) {
    Object r= null;
    switch (_col) {
      case 0 : //Si on affiche un temps
        final Double tmp= new Double((ini_.ordres[_row]).instant);
        //Conversion en string heure,min,sec
        r= FuLib.duration(tmp.longValue() * 1000);
        break;
      case 1 : // si on affiche un ordre
        r= ORDRES[(ini_.ordres[_row]).type];
        break;
      case 2 : //si on affiche la valeur d'un ordre
        //Interpretation des valeurs afin d'obtenir
        //un affichage plus parlant(en fonction du type d'ordres)
        r=
          interpreteValeur(
            (ini_.ordres[_row]).type,
            (ini_.ordres[_row]).valeur);
        break;
    }
    return r;
  }
  public String interpreteValeur(final int nordre, final double valeur) {
    String r= null;
    switch (nordre) {
      //Gouvernail
      case 0 :
        r= valeur + "�";
        break;
        //Pas Helice Babord
      case 1 :
        r= valeur * 100 + "%";
        //vieux hack mais c'est comme ca dans le code de calcul
        if (valeur > 1) {
          r= "100%";
        }
        if (valeur < -1) {
          r= "-100%";
        }
        break;
        //Pas Helice Tribord
      case 2 :
        r= valeur * 100 + "%";
        //vieux hack mais c'est comme ca dans le code de calcul
        if (valeur > 1) {
          r= "100%";
        }
        if (valeur < -1) {
          r= "-100%";
        }
        break;
        //Nombre de Tours Babord
      case 3 :
        r= valeur * 100 + "%";
        break;
        //Nombre de Tours Tribord
      case 4 :
        r= valeur * 100 + "%";
        break;
        //Force Propulseur Avant
      case 5 :
        r= valeur * 100 + "%";
        break;
        //Force Propulseur Arrri�re
      case 6 :
        r= valeur * 100 + "%";
        break;
        //Angle Propulseur Avant
      case 7 :
        r= valeur + "�";
        break;
        //Angle Propulseur Arri�re
      case 8 :
        r= valeur + "�";
        break;
    }
    return r;
  }
  /**
   * put your documentation comment here
   * @param _col
   */
  public String getColumnName(final int _col) {
    String r= null;
    //suivant le type de colonne
    switch (_col) {
      case 0 :
        r= "Temps";
        break;
      case 1 :
        r= "Type ordre";
        break;
      case 2 :
        r= "Valeur";
        break;
    }
    return r;
  }
  public void setValueAt(final Object _obj, final int _row, final int _col) {}
  public Class getColumnClass(final int _col) {
    Class r= Object.class;
    switch (_col) {
      case 0 :
        r= String.class;
        break;
      case 1 :
        r= String.class;
        break;
      case 2 :
        r= String.class;
        break;
    }
    return r;
  }
  /**
   * put your documentation comment here
   * @param _l
   */
  public void addTableModelListener(final TableModelListener _l) {}
  /**
   * put your documentation comment here
   * @param _l
   */
  public void removeTableModelListener(final TableModelListener _l) {}
}
