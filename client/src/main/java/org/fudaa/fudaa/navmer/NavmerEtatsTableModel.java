/*
 * @file         NavmerEtatsTableModel.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.fudaa.dodico.corba.navmer.SResultatsDAT;
import org.fudaa.dodico.corba.navmer.nombreRemorqueurs;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Nicola Maillot
 */
public class NavmerEtatsTableModel implements TableModel {
  private SResultatsDAT data_;
  private boolean[] colonnes_;
  private Vector listeners_;
  private String[] nomVariablesEtat_;
  public NavmerEtatsTableModel(final SResultatsDAT _data, final boolean[] _colonnes) {
    data_= _data;
    colonnes_= _colonnes;
    listeners_= new Vector(1, 1);
    nomVariablesEtat_= getNomsColonnesEtat();
  }
  //Nom des colonnes
  public static String[] getNomsColonnesEtat() {
    final String[] nomVariablesEtat= new String[33 + nombreRemorqueurs.value * 6];
    nomVariablesEtat[0]= "Temps";
    nomVariablesEtat[1]= "x";
    nomVariablesEtat[2]= "y";
    nomVariablesEtat[3]= "Cap";
    nomVariablesEtat[4]= "Vitesse longitudinale";
    nomVariablesEtat[5]= "Vitesse laterale";
    nomVariablesEtat[6]= "Vitesse rotationnelle";
    nomVariablesEtat[7]= "Ordre gouvernail";
    nomVariablesEtat[8]= "Ordre pas Helice babord";
    nomVariablesEtat[9]= "Ordre pas helice tribord";
    nomVariablesEtat[10]= "Ordre nombre de tours tribord";
    nomVariablesEtat[11]= "Ordre nombre de tours babord";
    nomVariablesEtat[12]= "Ordre force propulseur avant";
    nomVariablesEtat[13]= "Ordre force propulseur arri�re";
    nomVariablesEtat[14]= "Ordre angle propulseur Avant";
    nomVariablesEtat[15]= "Ordre angle propulseur Arri�re";
    nomVariablesEtat[16]= "Commande gouvernail";
    nomVariablesEtat[17]= "Commande pas helice babord";
    nomVariablesEtat[18]= "Commande pas helice tribord";
    nomVariablesEtat[19]= "Commande nombre de tours tribord";
    nomVariablesEtat[20]= "Commande nombre de tours babord";
    nomVariablesEtat[21]= "Commande force propulseur avant";
    nomVariablesEtat[22]= "Commande force propulseur arri�re";
    nomVariablesEtat[23]= "Commande angle propulseur avant";
    nomVariablesEtat[24]= "Commande angle propulseur arri�re";
    nomVariablesEtat[25]= "Angle d'incidence du vent";
    nomVariablesEtat[26]= "Module vent";
    nomVariablesEtat[27]= "Profondeur";
    nomVariablesEtat[28]= "Vitesse (x) courant moyenne";
    nomVariablesEtat[29]= "Vitesse (y) courant moyenne";
    nomVariablesEtat[30]= "Vitesse Rotationnelle moyenne";
    nomVariablesEtat[31]= "Distance berge babord";
    nomVariablesEtat[32]= "Distance berge tribord";
    for (int i= 0; i < nombreRemorqueurs.value; i++) {
      nomVariablesEtat[32 + i * 6 + 1]= "ordre position remorqueur " + i;
      nomVariablesEtat[32 + i * 6 + 2]= "ordre angle remorqueur " + i;
      nomVariablesEtat[32 + i * 6 + 3]= "ordre force remorqueur " + i;
      nomVariablesEtat[32 + i * 6 + 4]= "commande position remorqueur " + i;
      nomVariablesEtat[32 + i * 6 + 5]= "commande angle remorqueur " + i;
      nomVariablesEtat[32 + i * 6 + 6]= "commande force remorqueur " + i;
    }
    return nomVariablesEtat;
  }
  /**
   * @return : nombre de colonnes
   */
  public int getColumnCount() {
    int nbcol= 0;
    for (int i= 0; i < colonnes_.length; i++) {
      //System.err.println(colonnes_[i]);
      if (colonnes_[i]) {
        nbcol++;
      }
    }
    return nbcol;
  }
  /**
   * @return : nombre de lignes
   */
  public int getRowCount() {
    return Math.max(0, (data_.etats).length - 1);
  }
  public boolean isCellEditable(final int row, final int col) {
    return false;
  }
  public Object getValueAt(int row, final int col) {
    double r= 0.;
    row++; // sauter la premiere ligne
    //  (ent�te du fichier du sortie)
    final double[] ligne= new double[33 + nombreRemorqueurs.value * 6];
    ligne[0]= data_.etats[row].cinematique.instant;
    ligne[1]= data_.etats[row].cinematique.x;
    ligne[2]= data_.etats[row].cinematique.y;
    ligne[3]= data_.etats[row].cinematique.cap;
    ligne[4]= data_.etats[row].cinematique.vitesseLongitudinale;
    ligne[5]= data_.etats[row].cinematique.vitesseLaterale;
    ligne[6]= data_.etats[row].cinematique.vitesseRotationnelle;
    ligne[7]= data_.etats[row].ordre.gouvernail;
    ligne[8]= data_.etats[row].ordre.pasHeliceBabord;
    ligne[9]= data_.etats[row].ordre.pasHeliceTribord;
    ligne[10]= data_.etats[row].ordre.nombreToursBabord;
    ligne[11]= data_.etats[row].ordre.nombreToursTribord;
    ligne[12]= data_.etats[row].ordre.forcePropulseurAvant;
    ligne[13]= data_.etats[row].ordre.forcePropulseurArriere;
    ligne[14]= data_.etats[row].ordre.anglePropulseurAvant;
    ligne[15]= data_.etats[row].ordre.anglePropulseurArriere;
    ligne[16]= data_.etats[row].commande.gouvernail;
    ligne[17]= data_.etats[row].commande.pasHeliceBabord;
    ligne[18]= data_.etats[row].commande.pasHeliceTribord;
    ligne[19]= data_.etats[row].commande.nombreToursBabord;
    ligne[20]= data_.etats[row].commande.nombreToursTribord;
    ligne[21]= data_.etats[row].commande.forcePropulseurAvant;
    ligne[22]= data_.etats[row].commande.forcePropulseurArriere;
    ligne[23]= data_.etats[row].commande.anglePropulseurAvant;
    ligne[24]= data_.etats[row].commande.anglePropulseurArriere;
    ligne[25]= data_.etats[row].environnement.angleIncidenceVent;
    ligne[26]= data_.etats[row].environnement.moduleVent;
    ligne[27]= data_.etats[row].environnement.profondeur;
    ligne[28]= data_.etats[row].environnement.vitesseMoyenneX;
    ligne[29]= data_.etats[row].environnement.vitesseMoyenneY;
    ligne[30]= data_.etats[row].environnement.vitesseRotationnelleMoyenne;
    ligne[31]= data_.etats[row].environnement.distanceBergeBabord;
    ligne[32]= data_.etats[row].environnement.distanceBergeTribord;
    for (int i= 0; i < nombreRemorqueurs.value; i++) {
      ligne[32 + i * 6 + 1]= data_.etats[row].ordreRemorqueurs[i].position;
      ligne[32 + i * 6 + 2]= data_.etats[row].ordreRemorqueurs[i].angle;
      ligne[32 + i * 6 + 3]= data_.etats[row].ordreRemorqueurs[i].force;
      ligne[32 + i * 6 + 4]= data_.etats[row].commandeRemorqueurs[i].position;
      ligne[32 + i * 6 + 5]= data_.etats[row].commandeRemorqueurs[i].angle;
      ligne[32 + i * 6 + 6]= data_.etats[row].commandeRemorqueurs[i].force;
    }
    int idx= 0;
    for (int i= 0; i < ligne.length; i++) {
      if (colonnes_[i]) // ne traite que les colonnes activees
        {
        if (idx == col) {
          r= ligne[i];
        }
        idx++;
      }
    }
    return new Double(r);
  }
  /**
   * renvoie les noms des colonnes
   * @param col : numero de colonne
   */
  public String getColumnName(final int col) {
    int idx= 0;
    String nomcol= null;
    for (int i= 0; i < nomVariablesEtat_.length; i++) {
      if (colonnes_[i]) //Passe les colonnes non activ�es
        {
        if (idx == col) {
          nomcol= nomVariablesEtat_[i];
        }
        idx++;
      }
    }
    return nomcol;
  }
  public void setValueAt(final java.lang.Object obj, final int row, final int col) {}
  public Class getColumnClass(final int col) {
    return Double.class;
  }
  //permet l'export en fichier texte des resultats
  public void creeFichierTxt(final String _nomfic) {
    try {
      String s= "";
      final BufferedWriter fluxecriture= new BufferedWriter(new FileWriter(_nomfic));
      int i, j;
      //On ecrit d'abord les titres des colonnes dans le fichier
      s= getColumnName(0);
      final int nbcol= getColumnCount();
      for (i= 1; i < nbcol; i++) {
        s= s + ',' + getColumnName(i);
      }
      fluxecriture.write(s, 0, s.length());
      fluxecriture.newLine();
      //Puis les donn�es
      for (i= 0; i < getRowCount(); i++) {
        s= (getValueAt(i, 0)).toString();
        for (j= 1; j < nbcol; j++) {
          s= s + ',' + (getValueAt(i, j)).toString();
        }
        fluxecriture.write(s, 0, s.length());
        fluxecriture.newLine();
      }
      fluxecriture.close();
    } catch (final IOException _e) {
      System.err.println("Erreur pendant l'ecriture du fichier: " + _e);
    }
  }
  public void addTableModelListener(final TableModelListener _l) {
    listeners_.add(_l);
  }
  /**
   * put your documentation comment here
   * @param _l
   */
  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.remove(_l);
  }
}
