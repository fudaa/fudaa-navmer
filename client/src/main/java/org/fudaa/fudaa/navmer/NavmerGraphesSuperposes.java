/*
 * @file         NavmerGraphesSuperposes.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.PrintJob;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuPrintable;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.ebli.geometrie.GrPoint;
/**
 * Composant d'affichage de grpahiques superpos�s.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class NavmerGraphesSuperposes
  extends BuInternalFrame
  implements BuPrintable,  InternalFrameListener {
  //calque dont on recupere le morphisme versEcran
  NavmerImplementation imp_= null;
  //Coordonn�es de la trajectoire
  GrPoint[] points_= null;
  double[][] donnees_= null;
  String[] titres_;
  BuPanel graphes_;
  public NavmerGraphesSuperposes(
    final BuCommonImplementation _appli,
    final GrPoint[] _points,
  //points courbe
  final double[][] _donnees, final String[] _titres) {
    super("Graphe", true, true, true, true);
    points_= _points;
    donnees_= _donnees;
    titres_= _titres;
    imp_= (NavmerImplementation)_appli.getImplementation();
    creePanel();
    addInternalFrameListener(this);
    imp_.addInternalFrame(this);
  }
  NavmerGrapheResultats[] creeGraphes(final int _tailleimg) {
    final int nbcourbes= donnees_[0].length;
    final NavmerGrapheResultats[] graphes= new NavmerGrapheResultats[nbcourbes];
    final double[] valeursX= valeursX(); //axe des abscisses
    final String[] titre= new String[1];
    final double d[][]= new double[donnees_.length][1];
    for (int i= 0; i < nbcourbes; i++) {
      final Vector vy= new Vector();
      final GrPoint pt[]= pointsTransformes();
      for (int j= 0; j < donnees_.length; j++) {
        d[j][0]= donnees_[j][i];
        //recherche des points dont x est compris entre 0 et _tailleimg
        if (pt[j].x_ >= 0 && pt[j].x_ <= _tailleimg) {
          vy.add(new Double(donnees_[j][i]));
        }
      }
      double min, max;
      //recherche du y min et max des points precedents
      min= max= ((Double) (vy.elementAt(0))).doubleValue();
      double valeur;
      int u;
      for (u= 0; u < vy.size(); u++) {
        valeur= ((Double)vy.elementAt(u)).doubleValue();
        min= (valeur <= min) ? valeur : min;
        max= (valeur >= max) ? valeur : max;
      }
      titre[0]= titres_[i];
      graphes[i]= new NavmerGrapheResultats(d, valeursX, "X", titre);
      double tmp= min;
      if (min > max) {
        tmp= max;
        max= min;
        min= tmp;
      }
      graphes[i].getAxeY(0).minimum_= min;
      graphes[i].getAxeY(0).maximum_= max;
    }
    return graphes;
  }
  private GrPoint[] pointsTransformes() {
    //Determination du domaine de la courbe dans le calque
    final GrPoint[] pointstransformes= new GrPoint[points_.length];
    //        double[] valeursX=new double[points_.length];
    for (int i= 0; i < points_.length; i++) {
      pointstransformes[i]=
        points_[i].applique(imp_.getVueCalque().getCalque().getVersEcran());
    }
    return pointstransformes;
  }
  private double[] valeursX() {
    //Determination du domaine de la courbe dans le calque
    final GrPoint[] pointstransformes= pointsTransformes();
    final double[] valeursX= new double[points_.length];
    for (int i= 0; i < points_.length; i++) {
      valeursX[i]= pointstransformes[i].x_;
    }
    return valeursX;
  }
  //Methode qui creee les panels a partir de creeGraph
  public void creePanel() {
    setBackground(Color.white);
    graphes_= new BuPanel();
    graphes_.setLayout(new BuVerticalLayout(0, false, false));
    //On recupere une photo du calque pour l'afficher
    //au dessus des graphiques
    final BuPicture pic= new BuPicture(imp_.getVueCalque().getImageCache());
    final int wi= pic.getPreferredSize().width;
    //Decalage de margeGauche � gauche pour aligner les graphes.
    pic.setMode(BuPicture.SCALE);
    pic.setBorder(
      new EmptyBorder(
        0,
        NavmerGrapheResultats.margeGauche,
        0,
        NavmerGrapheResultats.margeDroite));
    final int wp= pic.getPreferredSize().width;
    graphes_.add(pic);
    final NavmerGrapheResultats[] g= creeGraphes(wi);
    //Calcul de la taille en hauteur des graphiques:
    //nombre de pixel a3 -
    //((espace entre composants)*(nb compsants) + taille calque)/nbcomposants
    final int nbpa3= 956;
    final int taillegraphe= (nbpa3 - pic.getPreferredSize().height) / g.length;
    for (int i= 0; i < g.length; i++) {
      g[i].getAxeX().minimum_= 0.;
      g[i].getAxeX().maximum_= wi;
      g[i].setPreferredSize(new Dimension(wp, taillegraphe));
      //temporaire:
      //on enleve la graduation X car la graduation est en pixels
      g[i].graduationsXEstVisible(false);
      graphes_.add(g[i]);
    }
    final JScrollPane sp= new JScrollPane(graphes_);
    graphes_.setBackground(Color.white);
    getContentPane().add(sp);
    sp.setPreferredSize(new Dimension(600, 400));
    pack();
  }
  /**
   * Autorise l'impression
   */
  public String[] getEnabledActions() {
    final String[] r= new String[] { "IMPRIMER", "PREVISUALISER", };
    return r;
  }
  /**
   * Methode d'impression d'un graphe
   */
  public void print(final PrintJob _job, final Graphics _g) {
    BuPrinter.INFO_DOC= new BuInformationsDocument();
    BuPrinter.INFO_DOC.name= getTitle();
    BuPrinter.INFO_DOC.logo= null;
    BuPrinter.printComponent(_job, _g, graphes_);
  }
  public void actionPerformed(final ActionEvent e) {}
  public void internalFrameClosed(final InternalFrameEvent e) {
    setVisible(false);
    imp_.removeInternalFrame(this);
  }
  public void internalFrameActivated(final InternalFrameEvent e) {}
  /**
   *  Non impl�ment�
   */
  public void internalFrameDeactivated(final InternalFrameEvent e) {}
  /**
   *  Non impl�ment�
   */
  public void internalFrameOpened(final InternalFrameEvent e) {}
  /**
   *  Non impl�ment�
   */
  public void internalFrameClosing(final InternalFrameEvent e) {}
  /**
   *  Non impl�ment�
   */
  public void internalFrameIconified(final InternalFrameEvent e) {}
  /**
   *  Non impl�ment�
   */
  public void internalFrameDeiconified(final InternalFrameEvent e) {}
}
