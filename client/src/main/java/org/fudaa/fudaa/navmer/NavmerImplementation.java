/*
 * @file         NavmerImplementation.java
 * @creation     2000-10-03
 * @modification $Date: 2007-05-04 13:59:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.memoire.bu.*;

import org.fudaa.ctulu.CsvReader;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.navmer.*;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.navmer.DCalculNavmer;
import org.fudaa.dodico.navmer.DParametresNavmer;
import org.fudaa.dodico.navmer.DResultatsNavmer;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrPolyligne;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.tableau.EbliFilleTableau;
import org.fudaa.ebli.trace.TraceSurface;

import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.aide.FudaaAidePreferencesPanel;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoMonitor;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaParamChangeLog;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
import org.fudaa.fudaa.navmer.these.AgIni;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * L'implementation du client Navmer.
 * 
 * @version $Revision: 1.21 $ $Date: 2007-05-04 13:59:31 $ by $Author: deniger $
 * @author Nicolas Maillot
 */
public class NavmerImplementation extends FudaaImplementation implements FudaaProjetListener {
  // public final static String LOCAL_UPDATE= ".";
  private NavmerFilleCalques ffCalques_; // Fenetre principale
  private BArbreCalque arbre_; // Arbre des calques
  public FudaaProjet projet_;
  private BuAssistant assistant_;
  private BuTaskView taches_;
  BGroupeCalque calqueDonnees_;
  private final static BuInformationsSoftware isNavmer_ = new BuInformationsSoftware();
  private final static BuInformationsDocument idNavmer_ = new BuInformationsDocument();
  public static ICalculNavmer SERVEUR_NAVMER;
  public static IConnexion CONNEXION_NAVMER;
  static {
    isNavmer_.name = "Navmer";
    isNavmer_.version = "1.0";
    isNavmer_.date = "19-oct-2000";
    isNavmer_.rights = "Tous droits r�serv�s. CETMEF (c)2000";
    isNavmer_.contact = "nmaillot@etu.utc.fr";
    isNavmer_.license = "GPL2";
    isNavmer_.languages = "fr";
    isNavmer_.logo = NavmerResource.NAVMER.getIcon("logo-navmer");
    isNavmer_.banner = NavmerResource.NAVMER.getIcon("banner-navmer");
    isNavmer_.http = "http://www.utc.fr/fudaa/navmer/";
    isNavmer_.update = "http://www.utc.fr/fudaa/distrib/deltas/navmer/";
    isNavmer_.man = FudaaLib.REMOTE_MAN;
    isNavmer_.authors = new String[] { "Nicolas Maillot", "Guillaume Desnoix" };
    isNavmer_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    isNavmer_.documentors = new String[] { "Yann Hollocou", "Son-Ha Lam" };
    isNavmer_.testers = new String[] { "Guillaume Desnoix", "Yann Hollocou", "Son-Ha Lam" };
    isNavmer_.libraries = new String[] { "Encodeurs: Jef Poskanzer\n    (http://www.acme.com/)",
        "Expressions r�guli�res: Wes Biggs\n    (http://www.cacas.org/~wes/java/)",
        "Aelfred: David Megginson\n (http://www.ncf.carleton.ca/~ak117/)",
        "Ic�nes 20x20: Dean S. Jones\n(http://www.javalobby.org/jfa/projects/icons/)",
        "Ic�nes 22x22: Rivyn\n    (http://rivyn.derkarl.org/kti/)",
        "Ic�nes 24x24: Tuomas Kuosmanen\n    (http://tigert.net/)", };
    idNavmer_.name = FudaaLib.getS("Etude");
    idNavmer_.version = "1";
    idNavmer_.logo = EbliResource.EBLI.getIcon("minlogo.gif");
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return isNavmer_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isNavmer_;
  }

  public BuInformationsDocument getInformationsDocument() {
    return idNavmer_;
  }

  /**
   * Modification de l'implementation de base BuCommonImplementation si necessaire.
   */
  public void init() {
    super.init();
    try {
      final BuSplashScreen ss = getSplashScreen();
      if (ss != null) {
        ss.setText("Ajustements");
        ss.setProgression(50);
      }
      final BuInformationsSoftware il = getInformationsSoftware();
      final BuInformationsDocument id = new BuInformationsDocument();
      setTitle(il.name + " " + il.version);
      BuPrinter.INFO_LOG = il;
      BuPrinter.INFO_DOC = id;
      final BuToolBar tb = getMainToolBar();
      tb.addSeparator();
      tb.addToolButton("Lancer le calcul...", "CALCULER", false);
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      final BuMenuBar mb = getMainMenuBar();
      mb.addMenu(buildCalculMenu());
      // mb.addMenu(buildAgentMenu());
      final BuMenu mi = (BuMenu) mb.getMenu("IMPORTER");
      mi.addMenuItem("Navire (*.NAV)", "IMPORTERNAV", true);
      mi.addMenuItem("Ordres (*.INI)", "IMPORTERINI", true);
      mi.addMenuItem("Courants (*.IDX)", "IMPORTERCOU", true);
      mi.addMenuItem("Contour de port (*.CSV)", "IMPORTERCSV", true);
      getApp().setEnabledForAction("CREER", true);
      getApp().setEnabledForAction("OUVRIR", true);
      getApp().setEnabledForAction("REOUVRIR", true);
      getApp().setEnabledForAction("IMPORTER", false);
      getApp().setEnabledForAction("QUITTER", true);
      getApp().setEnabledForAction("PREFERENCE", true);
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(NavmerPreferences.NAVMER);
        mr.setResource(NavmerResource.NAVMER);
      }
      mb.computeMnemonics();
      if (ss != null) {
        ss.setText("Bureau et assistant");
        ss.setProgression(60);
      }
      final BuMainPanel mp = getMainPanel();
      final BuColumn rc = mp.getRightColumn();
      assistant_ = new BuAssistant(); // NavmerAssistant
      rc.addToggledComponent(BuResource.BU.getString("Assistant"), "ASSISTANT", assistant_, this);
      taches_ = new BuTaskView();
      final BuScrollPane sp = new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp, this);
      mp.setLogo(il.logo);
      mp.setTaskView(taches_);
      mp.setAssistant(assistant_);
      // Si pas de code de calcul: on demande � l'utilisateur
      // de donner un chemin
      if (SERVEUR_NAVMER == null) {
        new BuDialogError(getApp(), isNavmer_, "Impossible de trouver l'executable du code de calcul.\n"
            + "Veuillez en definir un.").activate();
        // preferences().selectTab(6);
      }
    } catch (final Throwable t) {
      t.printStackTrace();
    }
  }

  public void start() {
    super.start();
    final BuInformationsSoftware il = getInformationsSoftware();
    assistant_.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "DEBUTER(" + il.name.toUpperCase()
        + ")"));
    BuPreferences.BU.applyOn(getApp());
    getMainPanel().getLeftColumn().setVisible(false);
    assistant_.changeAttitude(BuAssistant.PAROLE, BuResource.BU.getString("Bienvenue") + "!\n" + il.name + " "
        + il.version);
    JScrollPane sp;
    // Autorisation d'activation des actions gris�es par d�faut dans les menus
    // ou les toolbars
    // Panel pricipal
    final BuMainPanel mp = getApp().getMainPanel();
    // Colonne droite dans le panel pricipal. Contiendra le composant d'affichage
    // de l'arbre des calques
    final BuColumn rc = mp.getRightColumn();
    // L'application de base BuCommonImplementation ne contient pas �
    // l'origine de fenetre de visualisation d'objet graphiques.
    // Ajout d'une fenetre interne de visualisation des calques qui contiendra
    // les objets graphiques
    final BGroupeCalque gc = new BGroupeCalque();
    gc.setName("cqPRESENTATION");
    gc.setTitle(EbliResource.EBLI.getString("Pr�sentation"));
    mp.setProgression(15);
    final BuInformationsDocument id = getInformationsDocument();
    final BCalqueCartouche calque = new BCalqueCartouche();
    calque.setName("cqCARTOUCHE");
    calque.setTitle(FudaaLib.getS("Cartouche"));
    calque.setInformations(id);
    calque.setForeground(Color.black);
    calque.setBackground(new Color(255, 255, 224));
    calque.setFont(new Font("SansSerif", Font.PLAIN, 10));
    calque.setVisible(false);
    gc.add(calque);
    mp.setProgression(20);
    final BCalqueRosace calqueRosace = new BCalqueRosace();
    calqueRosace.setName("cqROSACE");
    calqueRosace.setTitle(FudaaLib.getS("Rosace"));
    calqueRosace.setForeground(Color.black);
    calqueRosace.setBackground(new Color(224, 224, 255));
    calqueRosace.setFont(new Font("SansSerif", Font.PLAIN, 10));
    calqueRosace.setVisible(false);
    gc.add(calqueRosace);
    mp.setProgression(30);
    final BCalqueDessin cd = new BCalqueDessin();
    cd.setName("cqDESSIN");
    cd.setTitle(FudaaLib.getS("Dessin"));
    final BCalqueDessinInteraction ci = new BCalqueDessinInteraction(cd);
    ci.setName("cqDESSIN_INTERACTION");
    ci.setTitle(FudaaLib.getS("Interaction"));
    ci.setGele(true);
    final BGroupeCalque grCq = new BGroupeCalque();
    grCq.setName("cqANNOTATIONS");
    grCq.setTitle(FudaaLib.getS("Annotations"));
    grCq.add(cd);
    grCq.add(ci);
    gc.add(grCq);
    calqueDonnees_ = new BGroupeCalque();
    calqueDonnees_.setName("cqDONNEES");
    calqueDonnees_.setTitle(EbliResource.EBLI.getString("Donn�es"));
    mp.setProgression(40);
    /*
     * { BCalqueDomaine calque = new BCalqueDomaine(); calque.setName("cqDOMAINE");
     * calque.setTitle(FudaaResource.getS("Domaine")); calque.setFont(new Font("SansSerif", Font.PLAIN, 8));
     * calque.setAttenue(false); calque.setVisible(false); calqueDonnees_.add(calque); calque.enDernier(); } {
     * BCalquePolygone calque = new BCalquePolygone(); calque.setName("cqZONES");
     * calque.setTitle(FudaaResource.getS("Zones")); calque.setFont(new Font("SansSerif", Font.PLAIN, 8));
     * calque.setAttenue(false); calque.setVisible(true); calque.setBackground(new Color(255, 224, 208));
     * calque.setForeground(new Color(224, 208, 192)); calque.setTypeSurface(TraceSurface.UNIFORME); GrPolygone p1 = new
     * GrPolygone(); p1.sommets.ajoute(new GrPoint(200., -350., 0.)); p1.sommets.ajoute(new GrPoint(308., -320., 0.));
     * p1.sommets.ajoute(new GrPoint(375., -420., 0.)); p1.sommets.ajoute(new GrPoint(200., -450., 0.));
     * calque.ajoute(p1); GrPolygone p2 = new GrPolygone(); p2.sommets.ajoute(new GrPoint(-220., 134., 0.));
     * p2.sommets.ajoute(new GrPoint(-120., 134., 0.)); p2.sommets.ajoute(new GrPoint(-90., 30., 0.));
     * p2.sommets.ajoute(new GrPoint(-130., 0., 0.)); p2.sommets.ajoute(new GrPoint(-227., 66., 0.)); calque.ajoute(p2);
     * calque.setVisible(false); calqueDonnees_.add(calque); calque.enPremier(); } { BCalqueIcone calque = new
     * BCalqueIcone(); calque.setName("cqBOUEES"); calque.setTitle(FudaaResource.getS("Bou�es")); calque.setFont(new
     * Font("SansSerif", Font.PLAIN, 8)); calque.setForeground(Color.red);
     * calque.setIcon(NavmerResource.NAVMER.getIcon("bouee-rouge")); calque.setDX(-12); calque.setDY(-24);
     * calque.ajoute(new GrPoint(308., -320., 0.)); calque.ajoute(new GrPoint(375., -420., 0.)); calque.ajoute(new
     * GrPoint(-120., 134., 0.)); calque.ajoute(new GrPoint(-90., 30., 0.)); calque.ajoute(new GrPoint(-130., 0., 0.));
     * calque.setVisible(false); calqueDonnees_.add(calque); calque.enPremier(); }
     */
    mp.setProgression(50);
    final double[][] r = new double[][] { { 10., 10., 10. }, { 5., 5., 5. }, { 0., 0., 0. }, { 0., 0., 0. } };
    mp.setProgression(60);
    // Arbre des calques
    arbre_ = new BArbreCalque();
    sp = new JScrollPane(arbre_);
    sp.setPreferredSize(new Dimension(150, sp.getPreferredSize().height));
    rc.addToggledComponent(BuResource.BU.getString("Calques"), "CALQUE", sp, this);
    mp.setProgression(70);
    // Fenetre des calques
    ffCalques_ = new NavmerFilleCalques(gc, arbre_);
    installContextHelp(ffCalques_.getRootPane(), "navmer/p-resultats-calques.html");
    mp.setProgression(80);
    final BVueCalque vc = ffCalques_.getVueCalque();
    // vc.setCalque(gc);
    vc.setBackground(Color.white);
    vc.setRepere(r);
    vc.setTaskView(taches_);
    vc.getCalque().add(calqueDonnees_);
    // ffCalques_.add
    arbre_.refresh();
    mp.setProgression(90);
    mp.getStatusBar().addMonitor(new FudaaDodicoMonitor());
    mp.setProgression(100);
    mp.setProgression(0);
  }

  public BVueCalque getVueCalque() {
    return ffCalques_.getVueCalque();
  }

  public static BuMenu buildCalculMenu() {
    final BuMenu r = new BuMenu(FudaaLib.getS("Calcul"), "CALCUL");
    r.addMenuItem(FudaaLib.getS("Lancer le calcul..."), "CALCULER", false, KeyEvent.VK_F9);
    r.addSeparator(FudaaLib.getS("Param�tres"));
    r.addMenuItem(NavmerResource.NAVMER.getString("R�ponse du navire au vent"), "REP_VENT", BuResource.BU
        .getMenuIcon("graphe"), false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Caracteristiques du navire"), "CARAC_NAVIRE", BuResource.BU
        .getMenuIcon("liste"), false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Tableau des ordres"), "TABLEAU_ORDRES", false);
    r.addSeparator(FudaaLib.getS("R�sultats"));
    r.addMenuItem(NavmerResource.NAVMER.getString("Tableau des r�sultats"), "TABLEAU_RESULTATS", false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Trajectoire"), "TRAJECTOIRE", EbliResource.EBLI
        .getMenuIcon("calque"), false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Cap"), "CAP", EbliResource.EBLI.getMenuIcon("calque"), false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Representation du vent"), "VENT", EbliResource.EBLI
        .getMenuIcon("calque"), false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Representation du courant par rapport au bateau"), "COURANTBATEAU",
        EbliResource.EBLI.getMenuIcon("calque"), false);
    r.addMenuItem(NavmerResource.NAVMER.getString("Representation des courants"), "COURANTS", EbliResource.EBLI
        .getMenuIcon("calque"), false);
    r.addSeparator(NavmerResource.NAVMER.getString("Analyse"));
    r.addMenuItem(NavmerResource.NAVMER.getString("Intersections"), "INTERSECTIONS", EbliResource.EBLI
        .getMenuIcon("calque"), false);
    r
        .addMenuItem(NavmerResource.NAVMER.getString("Dangers"), "DANGERS", EbliResource.EBLI.getMenuIcon("calque"),
            false);
    return r;
  }

  public static BuMenu buildAgentMenu() {
    final BuMenu r = new BuMenu(FudaaLib.getS("Agent"), "AGENT");
    r.setFont(BuLib.deriveFont("Menu", Font.ITALIC, 0));
    r.addMenuItem("Risques", "RISQUES", true);
    r.addMenuItem("Route", "ROUTE", true);
    r.addMenuItem("Genere ordres", "GENERE_ORDRES", true);
    return r;
  }

  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    // Commande d'ouverture
    if (action.equals("OUVRIR")) {
      cmdOuvrir();
    } else if (action.startsWith("REOUVRIR")) {
      // Le chemin du fichier � reouvrir est contenu dans action
      // il faut donc recupereri ce chemin � l'aide de substring
      cmdReOuvrir(action.substring(action.indexOf('(') + 1, action.indexOf(')')));
      // On empeche l'importation si il y a des resultats
      if (projet_ != null && projet_.getResult("ficdat") != null) {
        getApp().setEnabledForAction("IMPORTER", false);
      }
    } else if (action.equals("CREER")) {
      cmdCreer();
    } else if (action.equals("FERMER")) {
      cmdFermer();
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("PREFERENCE")) {
      preferences();
    } else if (action.equals("CALQUE")) {
      cmdSwitchToggleComponent(action);
    } else if (action.equals("IMPORTERNAV")) {
      importer(new String[] { "nav", "NAV" });
    } else if (action.equals("IMPORTERINI")) {
      importer(new String[] { "ini", "INI" });
    } else if (action.equals("IMPORTERCOU")) {
      importer(new String[] { "idx", "IDX" });
    } else if (action.equals("IMPORTERDAT")) {
      importer(new String[] { "dat", "DAT" });
    } else if (action.equals("IMPORTERCSV")) {
      importer(new String[] { "csv", "CSV" });
    } else if (action.equals("CALCULER")) {
      cmdCalculer();
    } else if (action.equals("TABLEAU_ORDRES")) {
      cmdTableauOrdres();
    } else if (action.equals("CARAC_NAVIRE")) {
      cmdCaracteristiquesNavire();
    } else if (action.equals("REP_VENT")) {
      cmdGrapheVent();
    } else if (action.equals("TRAJECTOIRE")) {
      cmdTrajectoire();
    } else if (action.equals("CAP")) {
      cmdCap();
    } else if (action.equals("COURANTS")) {
      cmdCourants();
    } else if (action.equals("VENT")) {
      cmdVent();
    } else if (action.equals("COURANTBATEAU")) {
      cmdCourantBateau();
    } else if (action.equals("TABLEAU_RESULTATS")) {
      cmdTableauResultats();
    } else if (action.equals("INTERSECTIONS")) {
      cmdIntersections();
    } else if (action.equals("DANGERS")) {
      cmdDangers();
    } else if (action.equals("RISQUES")) {
      cmdRisques();
    } else if (action.equals("ROUTE")) {
      cmdRoute();
    } else if (action.equals("GENERE_ORDRES")) {
      cmdGenereOrdres();
    } else {
      super.actionPerformed(_evt);
    }
  }

  // fudaaprojet listener
  public void dataChanged(final FudaaProjetEvent e) {
    switch (e.getID()) {
    case FudaaProjetEvent.RESULT_ADDED:
    case FudaaProjetEvent.RESULTS_CLEARED: {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      break;
    }
    }
  }

  public void statusChanged(final FudaaProjetEvent e) {
    if (e.getID() == FudaaProjetEvent.HEADER_CHANGED) {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
    }
  }

  /**
   * Affichage/masquage d'un toggle component de la colonne droite.
   */
  private void cmdSwitchToggleComponent(final String action) {
    final BuColumn rc = getMainPanel().getRightColumn();
    rc.toggleComponent(action);
    setCheckedForAction(action, rc.isToggleComponentVisible(action));
  }

  // Cette fonction active des menus apres une ouverture i
  // ou creation de projet
  private void activeMenusProjet(final boolean _b) {
    setEnabledForAction("ENREGISTRER", _b);
    setEnabledForAction("ENREGISTRERSOUS", _b);
    setEnabledForAction("EXPORTER", _b);
    setEnabledForAction("FERMER", _b);
    setEnabledForAction("IMPORTER", _b);
  }

  private void activeMenusCalcul(final boolean _b) {
    if (projet_.getParam("ficnav") != null) {
      setEnabledForAction("CARAC_NAVIRE", _b);
      setEnabledForAction("REP_VENT", _b);
    }
    if (projet_.getParam("ficini") != null) {
      setEnabledForAction("TABLEAU_ORDRES", _b);
    }
    if (projet_.getParam("ficidx") != null) {
      setEnabledForAction("COURANTS", _b);
    }
    if (projet_.getResult("ficdat") != null) {
      setEnabledForAction("TABLEAU_RESULTATS", _b);
      setEnabledForAction("TRAJECTOIRE", _b);
      setEnabledForAction("CAP", _b);
      setEnabledForAction("VENT", _b);
      if (projet_.getParam("ficidx") != null) {
        setEnabledForAction("COURANTBATEAU", _b);
      }
    }
    setEnabledForAction("INTERSECTIONS", _b);
    setEnabledForAction("DANGERS", _b);
    if (projet_.getParam("ficini") != null && projet_.getParam("ficnav") != null) {
      setEnabledForAction("CALCULER", _b);
    }
  }

  // On surcharge la methode suivante car dans BuCommonImplementation
  // lors de le fermeture d'une fenetre le menu FERMER est gris�
  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * Ouverture d'un fichier projet.
   */
  private void cmdOuvrir() {
    if (projet_ == null) {
      projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("navmer"));
    } else {
      cmdFermer();
    }
    projet_.setEnrResultats(true);
    projet_.ouvrir();
    if (projet_.estConfigure()) {
      // Active le chargement des resultats
      new BuDialogMessage(getApp(), isNavmer_, "Projet ouvert").activate();
      activeMenusCalcul(true);
      activeMenusProjet(true);
      final BuMenuBar mb = getMainMenuBar();
      mb.addRecentFile(projet_.getFichier(), "texte");
      mb.getMenu("REOUVRIR").setEnabled(true);
      NavmerPreferences.NAVMER.writeIniFile();
    }
  }

  private void cmdReOuvrir(final String _file) {
    System.out.println(_file);
    if (projet_ == null) {
      projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("navmer"));
    } else {
      cmdFermer();
    }
    projet_.setEnrResultats(true);
    projet_.ouvrir(_file);
    if (projet_.estConfigure()) {
      // Active le chargement des resultats
      new BuDialogMessage(getApp(), isNavmer_, "Projet ouvert").activate();
      activeMenusCalcul(true);
      activeMenusProjet(true);
    }
  }

  // Creation d'un projet
  private void cmdCreer() {
    if (projet_ == null) {
      projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("navmer"));
    } else {
      cmdFermer();
    }
    projet_.creer();
    projet_.addFudaaProjetListener(this);
    if (projet_.estConfigure()) {
      activeMenusProjet(true);
    }
  }

  public void cmdFermer() {
    if (projet_.estConfigure()) {
      final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), isNavmer_, "Voulez-vous enregistrer le projet");
      if (c.activate() == JOptionPane.YES_OPTION) {
        enregistrer(); // Sauvegarde si l'utilisateur le demande
      }
      activeMenusProjet(false);
      activeMenusCalcul(false);
      projet_.fermer();
      // On ferme toutes les fenetres du mainPanel
      final BuMainPanel mp = getMainPanel();
      JInternalFrame[] tabframe;
      tabframe = getMainPanel().getAllInternalFrames();
      calqueDonnees_.removeAll();
      for (int i = 0; i < tabframe.length; i++) {
        try {
          tabframe[i].setClosed(true);
        } catch (final PropertyVetoException _e) {
          _e.printStackTrace();
        }
        removeInternalFrame(tabframe[i]);
        mp.repaint();
      }
    }
  }

  // Prend en parametre un tableau d'extensions de fichier: ex: ["nav","NAV"]
  public void importer(final String[] ext) {
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new BuFileFilter(ext, "Fichiers " + ext[1]));
    final int returnVal = chooser.showOpenDialog((JFrame) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      Object obj = null;
      final String filename = chooser.getSelectedFile().getAbsolutePath();
      if (ext[0].equals("nav")) {
        projet_.addParam("ficnav", obj = DParametresNavmer.lectureFichierNav(filename));
      }
      if (ext[0].equals("ini")) {
        projet_.addParam("ficini", obj = DParametresNavmer.lectureFichierIni(filename));
      }
      if (ext[0].equals("idx")) {
        final SIndexZonesCourant index = DParametresNavmer.lectureIndexZonesCourant(filename);
        projet_.addParam("ficidx", index);
        projet_.addParam("ficcou", obj = DParametresNavmer.lectureFichierCou(filename.substring(0, filename
            .indexOf('.')), index));
      }
      if (ext[0].equals("dat")) {
        projet_.addParam("ficdat", obj = DResultatsNavmer.lectureFichierDat(filename));
        // Evite les incoherences
        getApp().setEnabledForAction("IMPORTER", false);
      }
      if (ext[0].equals("csv")) {
        final GrPolyligne polyligne = new GrPolyligne();
        try {
          final CsvReader rin = new CsvReader(new FileReader(filename));
          rin.readFields(2);
          while (true) {
            rin.readFields(2);
            final double x = rin.doubleField(0);
            final double y = rin.doubleField(1);
            polyligne.sommets_.ajoute(new GrPoint(x, y, 0.));
          }
        } catch (final EOFException ex) {
          ex.printStackTrace();
        } catch (final Exception ex) {
          ex.printStackTrace();
        }
        obj = polyligne;
        projet_.addParam("ficcsv", obj);
        GrBoite b = null;
        {
          final GrPolygone polygone = new GrPolygone();
          final int n = polyligne.nombre();
          for (int i = 0; i < n; i++) {
            polygone.sommets_.ajoute(polyligne.sommet(i));
          }
          b = polygone.boite();
          polygone.sommets_.ajoute(new GrPoint(b.e_.x_, b.e_.y_, 0.));
          polygone.sommets_.ajoute(new GrPoint(b.o_.x_, b.e_.y_, 0.));
          final BCalquePolygone calque = new BCalquePolygone();
          calque.setName("cqCONTOUR_PORT");
          calque.setTitle(FudaaLib.getS("Contour du port"));
          calque.setForeground(new Color(128, 96, 96));
          calque.setBackground(new Color(192, 160, 160));
          calque.setTypeSurface(TraceSurface.UNIFORME);
          calque.ajoute(polygone);
          ajouteCalque(calque);
          b = calque.getDomaine();
        }
        if (b != null) {
          final BCalqueGrille calque = new BCalqueGrille();
          calque.setName("cqGRILLE");
          calque.setTitle(FudaaLib.getS("Grille"));
          calque.setBoite(b);
          calque.setPasX(100.);
          calque.setPasY(100.);
          calque.setForeground(Color.lightGray);
          calque.setFont(new Font("SansSerif", Font.PLAIN, 8));
          calque.setAttenue(false);
          ajouteCalque(calque);
          calque.enDernier();
        }
      }
      System.err.println("obj:" + obj);
      new BuDialogMessage(getApp(), isNavmer_, "Param�tres " + ext[0] + " charg�").activate();
    }
    // On teste si tous les parametres on ete charges.
    // Il faut alors degriser l'option de lancement du calcul.
    // De m�me pour le menu exporter
    if (projet_.getParam("ficnav") != null && projet_.getParam("ficini") != null) {
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("EXPORTER", true);
    }
    // Si le fichier navire est charg� on active la possibilit�
    // de visualiser les caract�ristiques du navire
    if (projet_.getParam("ficnav") != null) {
      setEnabledForAction("CARAC_NAVIRE", true);
      setEnabledForAction("REP_VENT", true);
    }
    // Si le fichier d'ordres est charg� on active la possibilit�
    // de visualiser les ordres
    if (projet_.getParam("ficini") != null) {
      setEnabledForAction("TABLEAU_ORDRES", true);
    }
    // Si le calcul a �t� effectu� , on active la possibilit�
    // de visualiser les resultats
    if (projet_.getParam("ficdat") != null) {
      setEnabledForAction("TABLEAU_RESULTATS", true);
    }
  }

  protected void ajouteCalque(final BCalque _calque) {
    final BVueCalque vc = ffCalques_.getVueCalque();
    calqueDonnees_.add(_calque);
    _calque.enPremier();
    vc.invalidate();
    ffCalques_.validate();
    ffCalques_.repaint();
    arbre_.refresh();
  }

  public void cmdCalculer() {
    if (!isConnected()) {
      new BuDialogError(getApp(), isNavmer_, "Vous n'etes pas connect� � un serveur Navmer.").activate();
      return;
    }
    // Calcul du nombre d'ordres pour l'estimation du temps de calcul
    final BuTaskOperation op = new BuTaskOperation(this, "Calcul", "oprCalculer");
    // Calcul du nombre d'ordres pour aider � l'estimation
    // du temps decalcul
    final SParametresINI ini = (SParametresINI) projet_.getParam("ficini");
    final SZoneCourant[] zone = (SZoneCourant[]) projet_.getParam("ficcou");
    final int fact = (zone == null) ? 1 : 2;
    final int nbordres = ini.ordres.length;
    op.setEstimateDelay((nbordres / 15 * fact) * 1000);
    op.start();
  }

  public void oprCalculer() {
    setEnabledForAction("CALCULER", false);
    SParametresINI ini = null;
    SCoefficientsNavire coefnav = null;
    SIndexZonesCourant idxzones = null;
    SResultatsDAT data = null;
    SZoneCourant[] zone = null;
    // BuMainPanel mp = getMainPanel();
    // mp.setMessage("Transmission des parametres...");
    // mp.setProgression(0);
    ini = (SParametresINI) projet_.getParam("ficini");
    coefnav = (SCoefficientsNavire) projet_.getParam("ficnav");
    idxzones = (SIndexZonesCourant) projet_.getParam("ficidx");
    zone = (SZoneCourant[]) projet_.getParam("ficcou");
    final IParametresNavmer paramsNv = IParametresNavmerHelper.narrow(SERVEUR_NAVMER.parametres(CONNEXION_NAVMER));
    if (paramsNv == null) {
      System.err.println("ERROR: paramsNv null...");
    }
    // Initialisation des Parametres a partir des lectures precedentes
    paramsNv.parametresINI(ini);
    paramsNv.parametresNAV(coefnav);
    if (idxzones != null) {
      paramsNv.parametresIDX(idxzones);
      paramsNv.parametresCOU(zone);
    }
    System.err.println("Execution du calcul...");
    // mp.setMessage("Execution du calcul...");
    // mp.setProgression(20);
    try {
      SERVEUR_NAVMER.calcul(CONNEXION_NAVMER);
    } catch (final Throwable u) {
      u.printStackTrace();
      new BuDialogError(getApp(), isNavmer_, u.getMessage()).activate();
    }
    // mp.setProgression(100);
    setEnabledForAction("CALCULER", true);
    final IResultatsNavmer resultsNv = IResultatsNavmerHelper.narrow(SERVEUR_NAVMER.resultats(CONNEXION_NAVMER));
    data = resultsNv.resultatsDAT();
    projet_.addResult("ficdat", data);
    setEnabledForAction("CALCULER", true);
    activeMenusCalcul(true);
    // on empeche l'importation de nouveaux parametres afin d'eviter
    // toute incoherence
    getApp().setEnabledForAction("IMPORTER", false);
  }

  private SResultatsDAT getResultatsProjet() {
    if (projet_.getResult("ficdat") == null) {
      return null;
    }
    return (SResultatsDAT) projet_.getResult("ficdat");
  }

  private GrPolyligne getContourPort() {
    return (GrPolyligne) projet_.getParam("ficcsv");
  }

  private GrPolyligne getPolyligneTrajectoire() {
    final GrPolyligne r = new GrPolyligne();
    final SResultatsDAT data = getResultatsProjet();
    if (data != null) {
      System.err.println("Utilisation trajectoire calcul");
      for (int i = 1; i < data.etats.length; i++) {
        r.sommets_.ajoute(new GrPoint(data.etats[i].cinematique.x, data.etats[i].cinematique.y, 0.));
      }
    } else {
      r.sommets_.ajoute(new GrPoint(276., -384., 0.));
      r.sommets_.ajoute(new GrPoint(220., -303., 0.));
      r.sommets_.ajoute(new GrPoint(210., -260., 0.));
      r.sommets_.ajoute(new GrPoint(150., -200., 0.));
      r.sommets_.ajoute(new GrPoint(100., -75., 0.));
      r.sommets_.ajoute(new GrPoint(50., -15., 0.));
      r.sommets_.ajoute(new GrPoint(0., 0., 0.));
      r.sommets_.ajoute(new GrPoint(-130., 0., 0.));
      r.sommets_.ajoute(new GrPoint(-170., 50., 0.));
      r.sommets_.ajoute(new GrPoint(-150., 350., 0.));
      r.sommets_.ajoute(new GrPoint(-200., 400., 0.));
    }
    return r;
  }

  public void cmdTrajectoire() {
    final GrPolyligne trajectoire = getPolyligneTrajectoire();
    if (trajectoire == null) {
      return;
    }
    final BCalquePolyligne calque = new BCalquePolyligne();
    calque.setName("cqTRAJECTOIRE");
    calque.setTitle(FudaaLib.getS("Trajectoire"));
    calque.setForeground(Color.blue);
    calque.ajoute(trajectoire);
    ajouteCalque(calque);
    if (!(ffCalques_.isShowing())) {
      ffCalques_.setVisible(true);
      addInternalFrame(ffCalques_);
      ffCalques_.restaurer();
    }
  }

  public void cmdVent() {
    // Le vent est stock� dans les resultats, � chaque instant
    final SResultatsDAT data = getResultatsProjet();
    double x, y, mvent, avent;
    final BCalqueVecteur calque = new BCalqueVecteur();
    calque.setName("cqVent");
    calque.setForeground(Color.green);
    calque.setTitle(FudaaLib.getS("Vent"));
    ajouteCalque(calque);
    calque.setDensite(100);
    if (data != null) {
      for (int i = 0; i < data.etats.length; i++) {
        // A chaque position du bateau, representation du vecteur
        // vent
        x = data.etats[i].cinematique.x;
        y = data.etats[i].cinematique.y;
        mvent = data.etats[i].environnement.moduleVent;
        avent = data.etats[i].environnement.angleIncidenceVent;
        calque.ajoute(new GrPoint(x, y, 0.), new GrVecteur(100 * mvent * Math.cos(Math.toRadians(avent) + Math.PI / 2),
            100 * mvent * Math.sin(Math.toRadians(avent) + Math.PI / 2), 0.));
      }
      if (!(ffCalques_.isShowing())) {
        ffCalques_.setVisible(true);
        addInternalFrame(ffCalques_);
        ffCalques_.restaurer();
      }
    } else {
      // Si erreur d'acc�s aux courants
      new BuDialogError(getApp(), isNavmer_, FudaaResource.FUDAA
          .getString("Impossible d'acc�der aux resultats de la simulation")).activate();
    }
  }

  public void cmdCourantBateau() {
    // il semble y avoir un bug dans navmer: les valeurs de courant
    // sont incohrentes
    // Le vent est stock� dans les resultats, � chaque instant
    final SResultatsDAT data = getResultatsProjet();
    double x, y, vx, vy;
    final BCalqueVecteur calque = new BCalqueVecteur();
    calque.setName("cqCourantBateau");
    calque.setForeground(Color.black);
    calque.setTitle(FudaaLib.getS("Courant par rapport au bateau"));
    ajouteCalque(calque);
    calque.setDensite(100);
    if (data != null) {
      for (int i = 0; i < data.etats.length; i++) {
        // A chaque position du bateau, representation du vecteur
        // vent
        x = data.etats[i].cinematique.x;
        y = data.etats[i].cinematique.y;
        vx = data.etats[i].environnement.vitesseMoyenneX;
        vy = data.etats[i].environnement.vitesseMoyenneY;
        calque.ajoute(new GrPoint(x, y, 0.), new GrVecteur(100 * vx, 100 * vy, 0.));
      }
      if (!(ffCalques_.isShowing())) {
        ffCalques_.setVisible(true);
        addInternalFrame(ffCalques_);
        ffCalques_.restaurer();
      }
    } else {
      // Si erreur d'acc�s aux courants
      new BuDialogError(getApp(), isNavmer_, FudaaResource.FUDAA
          .getString("Impossible d'acc�der aux resultats de la simulation")).activate();
    }
  }

  // Representation du champ de courants
  public void cmdCourants() {
    if (projet_.getParam("ficcou") != null) {
      final BCalqueVecteur calque = new BCalqueVecteur();
      calque.setName("cqChampCourant");
      calque.setForeground(Color.red);
      calque.setTitle(FudaaLib.getS("Champ de Courants"));
      ajouteCalque(calque);
      calque.setDensite(100);
      final SZoneCourant[] zones = (SZoneCourant[]) projet_.getParam("ficcou");
      int j = 0;
      double normev = 0;
      double normemax = 0;
      double normeechelle = 0;
      // Pour chaque zone
      for (int i = 0; i < zones.length; i++) {
        // On prend chaque ligne de courant
        final SCinematiqueCourant[] cin = zones[i].cinematiques;
        // Calcul du pas: A priori constant en x et y:a verifier
        final double tgy = cin[1].y - cin[0].y;
        // Calcul du vecteur courant maximal
        for (j = 0; j < cin.length; j++) {
          normev = Math.sqrt(cin[j].vitesseX * cin[j].vitesseX + cin[j].vitesseY * cin[j].vitesseY);
          normemax = (normev >= normemax) ? normev : normemax;
        }
        for (j = 0; j < cin.length; j++) {
          // Calcul de la norme du vecteur
          normev = Math.sqrt(cin[j].vitesseX * cin[j].vitesseX + cin[j].vitesseY * cin[j].vitesseY);
          // Prise en compte de l'echelle
          normeechelle = normev * tgy / normemax;
          // Le vecteur est rendu unitaire puis remis � l'echelle
          calque.ajoute(new GrPoint(cin[j].x, cin[j].y, 0.), new GrPoint(cin[j].x + (cin[j].vitesseX / normev)
              * normeechelle, cin[j].y + (cin[j].vitesseY / normev) * normeechelle, 0.));
        }
      }
      if (!(ffCalques_.isShowing())) {
        ffCalques_.setVisible(true);
        addInternalFrame(ffCalques_);
        ffCalques_.restaurer();
      }
    } else {
      // Si erreur d'acc�s aux courants
      new BuDialogError(getApp(), isNavmer_, FudaaResource.FUDAA.getString("Impossible d'acc�der aux donn�es courants"))
          .activate();
    }
  }

  // Cette fonction ajoute un calque qui affiche le navire � differents
  // points de la simulation
  public void cmdCap() {
    // On recupere les caract du navire pour avoir ses dimensions
    final SCaracteristiquesNavire sc = DParametresNavmer.caracteristiquesNavire((SCoefficientsNavire) projet_
        .getParam("ficnav"));
    final SResultatsDAT data = getResultatsProjet();
    if (sc != null && data != null) {
      final GrPolygone bateau = new GrPolygone();
      final BCalquePolygone calque = new BCalquePolygone();
      // On defini un bateau unitaire
      bateau.sommets_.ajoute(new GrPoint(0.5, 0., 0.));
      bateau.sommets_.ajoute(new GrPoint(0.4, 0.5, 0.));
      bateau.sommets_.ajoute(new GrPoint(-0.5, 0.5, 0.));
      bateau.sommets_.ajoute(new GrPoint(-0.5, -0.5, 0.));
      bateau.sommets_.ajoute(new GrPoint(0.4, -0.5, 0.));
      calque.setName("cqCap");
      calque.setForeground(Color.gray);
      calque.setTitle(FudaaLib.getS("Cap"));
      calque.setTypeSurface(TraceSurface.UNIFORME);
      ajouteCalque(calque);
      if (data != null) {
        System.err.println("Utilisation trajectoire calcul");
        for (int i = 0; i < data.etats.length; i++) {
          GrPolygone bateauTransforme = new GrPolygone();
          // On applique une homotethie sur le bateau unitaire
          // Prise en compte de l'echelle?
          final double facteurechelle = 1.;
          bateauTransforme = bateau.applique(GrMorphisme.dilatation(sc.longueur * facteurechelle, sc.largeur
              * facteurechelle, 0.));
          // On applique on rotation qui correspond au cap
          bateauTransforme = bateauTransforme.applique(GrMorphisme.rotationZ(Math
              .toRadians(data.etats[i].cinematique.cap)));
          // On translate le bateau au point courant
          // bateauTransforme.applique
          bateauTransforme = bateauTransforme.applique(GrMorphisme.translation(data.etats[i].cinematique.x,
              data.etats[i].cinematique.y, 0.));
          calque.ajoute(bateauTransforme);
          calque.setDensite(100);
        }
      }
      if (!(ffCalques_.isShowing())) {
        ffCalques_.setVisible(true);
        addInternalFrame(ffCalques_);
        ffCalques_.restaurer();
      }
    } else {
      // Si erreur d'acc�s aux courants
      new BuDialogError(getApp(), isNavmer_, FudaaResource.FUDAA
          .getString("Impossible d'acc�der aux resultats de la simulation")).activate();
    }
  }

  public void cmdIntersections() {
    final GrPolyligne port = getContourPort();
    if (port == null) {
      return;
    }
    final GrPolyligne trajectoire = getPolyligneTrajectoire();
    if (trajectoire == null) {
      return;
    }
    final BCalquePoint calque = NavmerAnalyse.creeCalqueIntersections(port, trajectoire);
    ajouteCalque(calque);
    if (!(ffCalques_.isShowing())) {
      ffCalques_.setVisible(true);
      addInternalFrame(ffCalques_);
      ffCalques_.restaurer();
    }
  }

  public void cmdDangers() {
    final GrPolyligne port = getContourPort();
    if (port == null) {
      return;
    }
    final GrPolyligne trajectoire = getPolyligneTrajectoire();
    if (trajectoire == null) {
      return;
    }
    final BCalqueSegment calque = NavmerAnalyse.creeCalqueDangers(port, trajectoire, 20.);
    ajouteCalque(calque);
    if (!(ffCalques_.isShowing())) {
      ffCalques_.setVisible(true);
      addInternalFrame(ffCalques_);
      ffCalques_.restaurer();
    }
  }

  public void cmdRisques() {
    final GrPolyligne port = getContourPort();
    if (port == null) {
      return;
    }
    final GrPolyligne trajectoire = getPolyligneTrajectoire();
    if (trajectoire == null) {
      return;
    }
    final BCalqueSegment calque = NavmerAnalyse.creeCalqueRisques(port, trajectoire, 20.);
    ajouteCalque(calque);
    if (!(ffCalques_.isShowing())) {
      ffCalques_.setVisible(true);
      addInternalFrame(ffCalques_);
      ffCalques_.restaurer();
    }
  }

  public void cmdRoute() {
    final GrPolyligne port = getContourPort();
    if (port == null) {
      return;
    }
    final GrPolyligne trajectoire = getPolyligneTrajectoire();
    if (trajectoire == null) {
      return;
    }
    final GrPoint debut = trajectoire.sommet(0);
    final GrPoint fin = trajectoire.sommet(trajectoire.sommets_.nombre() - 1);
    GrPolyligne route = AgIni.creeRoute(debut, fin, port);
    {
      final BCalquePolyligne calque = new BCalquePolyligne();
      calque.setName("cqROUTE");
      calque.setTitle(FudaaLib.getS("Route"));
      calque.setForeground(new Color(0, 0, 192));
      calque.ajoute(route);
      calque.setVisible(false);
      ajouteCalque(calque);
    }
    {
      final BCalquePolyligne calque = new BCalquePolyligne();
      calque.setName("cqROUTE_LISSEE");
      calque.setTitle(FudaaLib.getS("Route liss�e"));
      calque.setForeground(new Color(0, 128, 0));
      for (int i = 0; i < 5; i++) {
        System.err.println("LISSE: " + i);
        route = AgIni.lisse(route);
      }
      calque.ajoute(route);
      ajouteCalque(calque);
      if (!(ffCalques_.isShowing())) {
        ffCalques_.setVisible(true);
        addInternalFrame(ffCalques_);
        ffCalques_.restaurer();
      }
    }
  }

  public void cmdGenereOrdres() {
    final GrPolyligne trajectoire = getPolyligneTrajectoire();
    if (trajectoire == null) {
      return;
    }
    final SPassageOrdre[] ordres = AgIni.genereOrdres(trajectoire);
    SParametresINI sini = (SParametresINI) projet_.getParam("ficini");
    if (sini == null) {
      sini = new SParametresINI();
    }
    sini.ordres = ordres;
    final GrPoint o = trajectoire.sommet(0);
    sini.entete.etatInitial.cinematique.x = o.x_;
    sini.entete.etatInitial.cinematique.y = o.y_;
    sini.entete.tempsDebut = 0.;
    sini.entete.tempsFin = ordres[ordres.length - 1].instant + 100.;
  }

  public void cmdTableauResultats() {
    final SResultatsDAT data = (SResultatsDAT) projet_.getResult("ficdat");
    if (data == null) {
      return;
    }
    boolean[] tab = new boolean[33 + nombreRemorqueurs.value * 6];
    // Affichage par defaut si aucun n'a �t� sp�cifi� par l'utilisateur
    if (projet_.getParam("colonnesres") == null) {
      for (int i = 0; i < 8; i++) {
        tab[i] = true;
      }
      for (int i = 10; i < 33 + nombreRemorqueurs.value * 6; i++) {
        tab[i] = false;
      }
    } else {
      tab = (boolean[]) projet_.getParam("colonnesres");
    }
    final EbliFilleTableau ff = new NavmerFilleTableauResultats(this, new NavmerEtatsTableModel(data, tab));
    installContextHelp(ff.getRootPane(), "navmer/p-resultats-tableau.html");
  }

  public void cmdTableauOrdres() {
    final EbliFilleTableau ff = new EbliFilleTableau();
    ff.setUtiliseFormules(false);
    final SParametresINI ini = (SParametresINI) projet_.getParam("ficini");
    if (ini == null) {
      return;
    }
    final BuTable table = new BuTable(new NavmerOrdresTableModel(ini));
    (table.getTableHeader()).setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    // On regle la taille des colonnes
    (table.getColumn(table.getColumnName(0))).setPreferredWidth(100);
    (table.getColumn(table.getColumnName(1))).setPreferredWidth(200);
    (table.getColumn(table.getColumnName(2))).setPreferredWidth(90);
    ff.setTable(table);
    ff.setPreferredSize(new Dimension(420, 500));
    ff.setSize(ff.getPreferredSize());
    addInternalFrame(ff);
    installContextHelp(ff.getRootPane(), "navmer/p-parametres-ordres.html");
  }

  public void cmdCaracteristiquesNavire() {
    final SCaracteristiquesNavire sc = DParametresNavmer.caracteristiquesNavire((SCoefficientsNavire) projet_
        .getParam("ficnav"));
    // decodage des caracteristiques du navire
    // type d'helice
    final String typehelice = (sc.typeHelice == 0) ? "H�lice(s) � pas fixe" : "H�lice(s) � pas variable";
    // type de propulseur
    String typeprop = null;
    switch (sc.configurationPropulseurs) {
    case 0:
      typeprop = "Pas de propulseur";
      break;
    case 1:
      typeprop = "Un propulseur � l'avant";
      break;
    case 2:
      typeprop = "Un propulseur � l'avant et � l'arri�re";
    }
    // Charge du navire
    String chargenav = null;
    switch (sc.chargeNavire) {
    case 0:
      chargenav = "Pleine charge";
      break;
    case 1:
      chargenav = "Ballast";
      break;
    case 2:
      chargenav = "L�ger";
      break;
    case 3:
      chargenav = "Sans conteneurs";
      break;
    case 4:
      chargenav = "Avec conteneurs";
    }
    // Type de navire
    String typenav = null;
    switch (sc.typeNavire) {
    case 0:
      typenav = "Navire rapide 3 jets";
      break;
    case 1:
      typenav = "Navire rapide 4 jets";
      break;
    case 2:
      typenav = "Cargo";
      break;
    case 3:
      typenav = "Container";
      break;
    case 4:
      typenav = "Convoi pouss�";
      break;
    case 5:
      typenav = "Ferry";
      break;
    case 6:
      typenav = "Methanier";
      break;
    case 7:
      typenav = "Paquebot";
      break;
    case 8:
      typenav = "Petrolier";
      break;
    case 9:
      typenav = "Vracquier";
    }
    // On construit une chaine qui va �tre plac�e dans le
    // BuMultiLabel
    final String caract = new String("Longueur (m): " + sc.longueur + CtuluLibString.LINE_SEP_SIMPLE + "Largeur (m): "
        + sc.largeur + CtuluLibString.LINE_SEP_SIMPLE + "Tirant (m): " + sc.tirant + CtuluLibString.LINE_SEP_SIMPLE
        + "Nombre de tours maximal: " + sc.nombreToursMaximal + CtuluLibString.LINE_SEP_SIMPLE
        + "Vitesse maximale (noeuds): " + sc.vitesseMaximale + CtuluLibString.LINE_SEP_SIMPLE
        + "Pas d'helice maximal (mm): " + sc.pasHeliceMaximal + CtuluLibString.LINE_SEP_SIMPLE
        + "Nombre de gouvernails: " + sc.nombreGouvernails + CtuluLibString.LINE_SEP_SIMPLE + "Nombre d'h�lices: "
        + sc.nombreHelices + CtuluLibString.LINE_SEP_SIMPLE + "Type d'helice: " + typehelice
        + CtuluLibString.LINE_SEP_SIMPLE + "Configuration des propulseurs: " + typeprop
        + CtuluLibString.LINE_SEP_SIMPLE + "Charge du navire: " + chargenav + CtuluLibString.LINE_SEP_SIMPLE
        + "Type de navire: " + typenav + CtuluLibString.LINE_SEP_SIMPLE);
    final BuInternalFrame buframe = new BuInternalFrame("Caracteristiques Navire", true, true, true, true);
    final BuLabelMultiLine lab = new BuLabelMultiLine(caract);
    buframe.getContentPane().setLayout(new BorderLayout());
    buframe.getContentPane().add(lab, BorderLayout.CENTER);
    buframe.setPreferredSize(new Dimension(500, 300));
    buframe.setBackground(Color.white);
    buframe.pack();
    buframe.addInternalFrameListener(this);
    addInternalFrame(buframe);
    installContextHelp(buframe.getRootPane(), "navmer/p-parametres-carac.html");
  }

  public void cmdGrapheVent() {
    final double[] ventx = ((SCoefficientsNavire) projet_.getParam("ficnav")).ventX;
    final double[] venty = ((SCoefficientsNavire) projet_.getParam("ficnav")).ventY;
    final double[] ventn = ((SCoefficientsNavire) projet_.getParam("ficnav")).ventN;
    final double[][] tabreponsevent = new double[19][3];
    // On construit un tableau � deux dimension pour pouvoir le
    // passer au NavmerGrapheResultats
    int i;
    for (i = 0; i < 19; i++) {
      tabreponsevent[i][0] = ventx[i];
      tabreponsevent[i][1] = venty[i];
      tabreponsevent[i][2] = ventn[i];
    }
    // Axe x: Pas de 10�
    final double[] axex = new double[19];
    for (i = 0; i < 19; i++) {
      axex[i] = i * 10;
    }
    // Titre des courbes
    final String[] nomcourbe = { "Force en tonnes (X)", "Force en tonnes (Y)", "Moment en tonnes.m�tre" };
    final NavmerGrapheResultats ng = new NavmerGrapheResultats(tabreponsevent, axex, "angle d'incidence (�)", nomcourbe);
    final NavmerFilleGraphe g = new NavmerFilleGraphe(this, "R�ponse du navire au vent");
    g.setGraphe(ng);
    installContextHelp(g.getRootPane(), "navmer/p-parametres-vent.html");
  }

  public void enregistrerSous() {
    final String[] ext = { "navmer", "NAVMER" };
    // Choix du fichier � ecrire
    final BuFileChooser chooser = new BuFileChooser();
    // On cr�e un filtre
    chooser.setFileFilter(new BuFileFilter(ext, "Fichiers navmer"));
    final int returnVal = chooser.showSaveDialog((JFrame) getApp());
    // test si l'utilisateur a valid�
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      final String filename = chooser.getSelectedFile().getAbsolutePath();
      projet_.setEnrResultats(true); // Sauvegarde des resultats activ�e
      projet_.setFichier(filename);
      projet_.enregistreSous();
      // Activation de l'enregistrement
      setEnabledForAction("ENREGISTRER", true);
      // ajout du projet aux fichiers recents
      final BuMenuBar mb = getMainMenuBar();
      mb.addRecentFile(projet_.getFichier(), "texte");
      mb.getMenu("REOUVRIR").setEnabled(true);
      NavmerPreferences.NAVMER.writeIniFile();
    }
  }

  public void enregistrer() {
    projet_.enregistre();
    // Ajout du projet aux fichiers recents
    final BuMenuBar mb = getMainMenuBar();
    mb.addRecentFile(projet_.getFichier(), "texte");
    mb.getMenu("REOUVRIR").setEnabled(true);
    NavmerPreferences.NAVMER.writeIniFile();
  }

  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuLookPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuUserPreferencesPanel(this));
    _prefs.add(new FudaaAidePreferencesPanel(this, NavmerPreferences.NAVMER));
    _prefs.add(new NavmerPreferencesPanel(this));
  }

  public void exit() {
    closeConnexions();
    super.exit();
  }

  public void finalize() {
    closeConnexions();
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return NavmerPreferences.NAVMER;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_NAVMER = null;
    SERVEUR_NAVMER = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_NAVMER, CONNEXION_NAVMER);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculNavmer.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculNavmer.class);
    CONNEXION_NAVMER = c.getConnexion();
    SERVEUR_NAVMER = ICalculNavmerHelper.narrow(c.getTache());
  }
}
