/*
 * @file         AgNavmer.java
 * @creation     2000-10-03
 * @modification $Date: 2006-09-19 15:11:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer.these;
import com.memoire.bu.BuApplication;
import com.memoire.bu.BuPreferences;

import org.fudaa.dodico.corba.navmer.ICalculNavmerHelper;

import org.fudaa.dodico.navmer.DParametresNavmer;
import org.fudaa.dodico.objet.CDodico;

import org.fudaa.fudaa.navmer.NavmerImplementation;
/**
 * Classe de lancement de l'application Navmer. Contient la methode main
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:11:55 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class AgNavmer {
  public static void main(final String[] args) {
    if (NavmerImplementation.SERVEUR_NAVMER == null) {
      System.out.println("Recherche d'un serveur navmer...");
      NavmerImplementation.SERVEUR_NAVMER=
        ICalculNavmerHelper.narrow(
          CDodico.findServerByInterface("::navmer::ICalculNavmer"));
      System.out.println("--> " + NavmerImplementation.SERVEUR_NAVMER);
    }
    BuPreferences.BU.applyLookAndFeel();
    // Cr�ation de l'application
    final BuApplication app= new BuApplication();
    final NavmerImplementation imp= new NavmerImplementation();
    app.setImplementation(imp);
    app.init();
    try {
      Thread.sleep(500);
    } catch (final Exception ex) {}
    // D�marrage de l'application
    app.start();
    String filename;
    filename= "/home/desnoix/_NAVMER_EXEMPLES/ferry.nav";
    imp.projet_.addParam(
      "ficnav",
      DParametresNavmer.lectureFichierNav(filename));
    filename= "/home/desnoix/_NAVMER_EXEMPLES/ferry.ini";
    imp.projet_.addParam(
      "ficini",
      DParametresNavmer.lectureFichierIni(filename));
    imp.setEnabledForAction("CALCULER", true);
    imp.setEnabledForAction("TABLEAU_ORDRES", true);
    System.err.println("MAIN FINISHED");
  }
}
