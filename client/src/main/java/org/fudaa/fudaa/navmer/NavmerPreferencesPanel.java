/*
 * @file         NavmerPreferencesPanel.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;

import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
/**
 * Panneau de preferences pour Navmer.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class NavmerPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener {
  NavmerPreferences options_;
  NavmerImplementation navmer_;
  protected BuTextField tfDirOpen_;
  protected BuButton btDirOpen_;
  public String getTitle() {
    return "Navmer";
  }
  // Constructeur
  public NavmerPreferencesPanel(final NavmerImplementation _navmer) {
    super();
    options_= NavmerPreferences.NAVMER;
    navmer_= _navmer;
    final BuBorderLayout lo= new BuBorderLayout();
    lo.setVgap(5);
    lo.setHgap(5);
    final BuPanel p= new BuPanel();
    //p.setBorder(new TitledBorder(getTitle()));
    p.setLayout(lo);
    p.setBorder(
      new CompoundBorder(
        new TitledBorder("Choix de l'executable"),
        new EmptyBorder(5, 5, 5, 5)));
    tfDirOpen_= new BuTextField(options_.getStringProperty("execpath"));
    tfDirOpen_.setEditable(false);
    //Bouton qui provoque l'ouverture d'un filechooser
    btDirOpen_= new BuButton();
    btDirOpen_.setIcon(BuResource.BU.getIcon("executer"));
    btDirOpen_.setMargin(new Insets(0, 0, 0, 0));
    btDirOpen_.addActionListener(this);
    btDirOpen_.setActionCommand("OUVRIR");
    p.add(tfDirOpen_, BuBorderLayout.CENTER);
    p.add(btDirOpen_, BuBorderLayout.EAST);
    setLayout(new BuBorderLayout());
    setBorder(new EmptyBorder(5, 5, 5, 5));
    add(p, BuBorderLayout.NORTH);
  }
  // Evenements
  public void actionPerformed(final ActionEvent _evt) {
    if (_evt.getActionCommand().equals("OUVRIR")) {
      //On recherche le type de l'OS
      final String os= System.getProperty("os.name");
      String nomexec;
      //Definition des nom des executables a lancer selon l'OS
      if (os.startsWith("Win")) {
        nomexec= "navmer-Win95.exe";
      } else {
        nomexec= "navmer-Linux.x";
      }
      final BuFileChooser chooser= new BuFileChooser();
      chooser.setFileFilter(new FudaaFiltreFichier(nomexec));
      final int returnVal= chooser.showOpenDialog((JFrame)navmer_.getApp());
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        //On met � jour le champ texte apr�s avoir
        //choisi un nouveau fichier
        final String filename= chooser.getSelectedFile().getAbsolutePath();
        tfDirOpen_.setText(filename);
        //il faut ecrire dans .navmerrc ce parametre
        options_.putStringProperty("execpath", filename);
      }
    }
  }
  // Methodes publiques
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return false;
  }
  public void applyPreferences() {
    options_.applyOn(navmer_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
  }
}
