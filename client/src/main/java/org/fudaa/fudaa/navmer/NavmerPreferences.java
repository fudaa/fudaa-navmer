/*
 * @file         NavmerPreferences.java
 * @creation     2000-10-06
 * @modification $Date: 2006-09-19 15:11:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.navmer;
import com.memoire.bu.BuPreferences;
/**
 * put your module comment here
 * Preferences pour Navmer.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 15:11:54 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class NavmerPreferences extends BuPreferences {
  public final static NavmerPreferences NAVMER= new NavmerPreferences();
}
